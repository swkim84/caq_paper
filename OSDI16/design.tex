%\blockquote{\textit{"... There are always cleaners and compaction threads that need to do I/O, but shouldn't hold off the higher-priority "foreground" I/O. ... Facebook really only needs two (or few) priority levels: low and high."}}
%In this paper, without loss of generality, we use task to refer to a single thread of execution in a computing system, being it either a thread in a multi-threaded application, or a process.
\vspace{-0.1in}
\section{Our Approach}\label{sec:Approach}
\vspace{-0.1in}

As we explained in Section~\ref{sec:Motivation}, the response time of request execution determines the level of application performance. In this respect, we classify I/Os into two priority levels: (performance) \textit{critical} and \textit{non-critical} I/Os. In particular, we define a critical I/O as an I/O awaited in the context of request execution directly or indirectly.

\begin{figure}
\centering
\epsfig{file=../figures/io-classification.eps, width=2.5in}
\vspace{-0.1in}
\caption{\textbf{Comparison with conventional I/O classification schemes.} \textit{}}
\vspace{-0.2in}
\label{fig:io-classification}
\end{figure}

The proposed classification is distinguished from the conventional I/O classification schemes: \textit{I/O-centric} and \textit{task-centric} classifications (Figure~\ref{fig:io-classification}). The I/O-centric classification differentiates the importance of each I/O based on its operation type (e.g., synchronous I/Os over asynchronous ones~\cite{Ganger1993,CFQ,Jeong2015}) for improving system responsiveness. On the other side, the task-centric classification distinguishes the I/Os from each task for applying task-level I/O prioritization (e.g., foreground I/Os over background ones~\cite{Yang2008}). These static classification schemes, however, are inadequate for identifying I/O criticality. In our request-centric viewpoint, synchronous I/Os (e.g., checkpoint writes) can be non-critical and asynchronous I/Os can become critical due to the I/O dependency. Moreover, not all foreground I/Os are critical because buffered writes issued by a foreground task can be asynchronously processed by a kernel thread.
 
Based on the I/O criticality classification, we introduce a \textit{request-centric I/O prioritization} (or RCP for short) that identifies and prioritizes critical I/Os over non-critical ones along the I/O path. This form of two-level I/O prioritization is effective for many cases since background tasks are ubiquitous in practice. For example, according to a Facebook developer: \enquote{\textit{... There are always cleaners and compaction threads that need to do I/O, but shouldn't hold off the higher-priority "foreground" I/O. ... Facebook really only needs two (or few) priority levels: low and high.}}~\cite{LSFMM2014a}.

Our goals for realizing RCP are twofold: 1) requiring no or little application modification for detecting and prioritizing critical I/Os based on common characteristics in modern applications, and 2) processing non-critical I/Os in a best-effort manner while minimizing the interference to foreground tasks. In the following section, we describe our design for effectively detecting and enforcing I/O criticality along the I/O path keeping these goals in mind.

\vspace{-0.1in}
\section{I/O Path Enlightenment}\label{sec:Design}

\vspace{-0.05in}
\subsection{Enlightenment API}\label{sec:Design1}

In order to accurately identify critical I/Os, we need to track a set of tasks (i.e., foreground tasks) involved in request execution. We have two options to this end: system-level and application-guided approaches. Basically, a system-level approach infers foreground tasks based on observable information at the kernel. Though the system-level approach has the benefit of avoiding application modification, it may induce huge engineering effort and runtime overhead to the kernel for inferring foreground tasks and reducing the possibility of misidentification. Moreover, the kernel may fail to detect a foreground task if an application uses shared memory as a way of inter-process communication among cooperative tasks. In contrast, applications can accurately guide which tasks are involved in request execution without runtime overheads at the expense of application modifications.
 
We chose the application-guided approach for accurate detection of request contexts without burdening the OS kernel. In particular, we provide an enlightenment interface to user-level so that an application developer (or an administrator if possible) can dynamically set/clear request context to a task based on application-level semantics. The request context can be from a short code section to an entire life of a thread depending on where set/clear APIs are called. The simplicity of using APIs makes developers easily prioritize critical I/Os in their applications. We believe the modification cost is also low in practice because typical data-intensive applications already distinguish foreground tasks from background tasks; see Section~\ref{sec:Applications} for details. %For an event-driven application that uses single process/thread to handle both foreground and background tasks, the provided interface can be used in a more fine-grained manner (i.e., request-level tagging). Furthermore, an administrator (or a user-level script) can tag a set of foreground tasks using the \texttt{nice} or \texttt{ionice} utility even without application modifications if an application exposes any comprehensive information by means of task names, tools, documentation, and so forth.

Since a hint is solely used for deciding I/O criticality, a wrong hint does not affect the correct execution of an application. However, hint abuse by a malicious or a thoughtless application/tenant may compromise performance isolation among multiple applications/tenants sharing a storage stack. This problem can be solved by integrating the request-centric I/O prioritization to group-based resource management (e.g., cgroup in Linux~\cite{Menage2007}). Addressing the issue is out of scope of this paper.

\vspace{-0.1in}
\subsection{I/O Priority Inheritance}\label{sec:Design2}

% Dependency-induced critical I/Os
Based on the enlightenment interface, we basically regard a synchronous I/O issued within a request context as a critical I/O. This simple identification alone, however, is insufficient for accurately detecting all critical I/Os because a dependency can make a background I/O delay the progress of request execution (Section~\ref{sec:Challenge2}). Hence, the next step for critical I/O detection is to handle I/O priority inversion caused by runtime dependencies. To this end, we introduce \textit{I/O priority inheritance} that temporarily gives critical I/O priority to a background task (Section~\ref{sec:Design2-1}) or a background I/O (Section~\ref{sec:Design2-2}) on which a foreground task depends to make progress. In the followings, we detail the mechanisms for detecting the dependency-induced critical I/Os.

\vspace{-0.1in}
\subsubsection{Handling Task Dependency}\label{sec:Design2-1}

% I/O priority inheritance on kernel-level synchronizations
\textbf{Kernel-level dependency.} Resolving the lock-induced dependency has been well-studied in the context of task scheduling~\cite{Lampson1980}. Inspired by the previous work, we resolve the lock-induced dependency by inheriting critical I/O priority to a background task when it blocks a foreground task until it leaves a critical section. Specifically, we record an owner task into each lock object (e.g., mutex). When a task is blocked to acquire a lock, the lock owner inherits I/O priority of the waiting task. The inherited priority is revoked when the owner task exits the critical section. Note that we solely consider blocking-based locks in this work because spinning-based locks are intended to very short critical sections in which no I/O activity is involved.

Unlike the case of locks, there is no distinct owner in cond-based synchronizations at the time of dependency occurs. To deal with the cond-induced dependency, we use a similar approach to the priority inheritance on condition variables introduced in the context of CPU scheduling~\cite{Cucinotta2009}. In particular, a task is registered as a \textit{helper task}~\cite{Cucinotta2009} into the corresponding cond object when the task is going to satisfy the condition. Later, the helper task inherits I/O priority of a task blocked to wait for the condition become true. The inherited I/O priority is revoked when the helper task finally signals the waiting task.

% I/O priority inheritance on user-level synchronizations
\textbf{User-level dependency.} The key challenge for handling the user-level dependency is that the OS kernel cannot clearly identify a dependency relationship resulted from user-level synchronizations. For user locks, the kernel cannot determine the owner because a user lock variable is located in a shared memory region and modified through atomic operations (e.g., \texttt{cmpxchg} in x86) to indicate the lock status. This is an intrinsic optimization to eliminate unnecessary kernel interventions in the uncontended cases. The kernel is involved only when to block or to wake up lock waiters via a system call (e.g., \texttt{sys\_futex()}). As a consequence, the OS kernel can see only the waiter failed to acquire a user-level lock.

To accurately detect the owner of a user lock, we adjust a user lock primitive (e.g., \texttt{pthread\_mutex\_lock()}) to additionally pass down the information of the owner when a task should block in the kernel due to lock contention. In practice, this can be readily done without modifying existing interfaces; see Section~\ref{sec:Implementation} for our implementation. Based on the delivered information, the kernel can properly inherit the I/O priority of waiting task to a user-level lock-holder task. This modification does not entail additional system calls in the uncontended cases. 

A possible solution for handling the user cond-induced dependency would be to provide additional interface for an application developer to properly tag a helper task for each user cond. However, this approach is impractical because modern applications extensively use user conds for various purposes. For instance, we found over a hundred of user conds in the source code of MongoDB. Therefore, properly identifying all helper tasks is not a trivial task even for an application developer.

We adopt a kernel-level inference technique that identifies a helper task based on history for each user cond. Typically, a background task is dedicated to a specific activity like logging, checkpointing, compaction, and buffer cleaning. Hence, a task signaling a condition is highly likely signal the condition again. A background task is registered as a helper task when it signals a user cond. Then, the helper task inherits critical I/O priority of a foreground task when the foreground task needs to block on the user cond. The helper task is unregistered when it does not signal again for a specific time window.

\vspace{-0.1in}
\subsubsection{Handling I/O Dependency}\label{sec:Design2-2}

% Difficulty
Properly resolving a dependency to an outstanding I/O is complicated because the dependent I/O can be in any stage in the block layer at the time of the dependency occurs. For example, an outstanding I/O can be in admission control stage waiting for the available slots of a block-level queue. Hence, we need to track the status of an ongoing non-critical I/O and appropriately reprioritize it according to the current location when required.

% I/O Tracking
For tracking outstanding non-critical I/Os, we add an \texttt{ncio} data structure that stores an I/O descriptor, current location, and the descriptor of a requesting task. An \texttt{ncio} object is allocated when an incoming I/O is classified as non-critical at the entrance of the block layer, and inserted to a per-device list sorted by starting sector number. The fields including I/O descriptor and current location in the \texttt{ncio} object are properly updated as the corresponding I/O flows along the I/O path. The allocated \texttt{ncio} object is freed when the corresponding I/O is reclassified or dispatched to a device. Note that we check whether a dependency occurs before entering the block layer or not for an incoming I/O before classifying it as non-critical I/O.

% Resolution
When a dependency to an ongoing non-critical I/O occurs, the per-device \texttt{ncio} list is searched to find the corresponding \texttt{ncio} object. Then, the non-critical I/O is reclassified as critical I/O based on the information in the I/O descriptor stored in an \texttt{ncio} object if the corresponding \texttt{ncio} object is found. In this case, we may need to conduct additional chores according to the current location of the dependent I/O; we present the details in the following subsection.

% Overhead
The runtime overhead for maintaining the ncio structure is fairly small. In our implementation, we used a red-black tree to speed up the lookup. The memory cost is also limited because the number of outstanding non-critical I/Os is limited (128 by default) by the admission control mechanism at the block layer.

\vspace{-0.1in}
\subsection{Criticality-Aware I/O Processing}\label{sec:Design3}

As we discussed in Section~\ref{sec:Challenge1}, prioritizing critical I/Os only within a single layer (e.g., scheduling in a block-level queue) is ineffective for improving application performance. Hence, we adapt each layer in the I/O path to make it understand and enforce the I/O criticality.

% Caching layer
In the caching layer, similar to the approach in \cite{Yang2008}, we apply separate dirty ratios to tasks issuing critical and non-critical writes, respectively. For tasks issuing non-critical writes, the applied dirty ratio is low (1\% by default) to mitigate the interference to foreground tasks. With the lowered limit, a background task writing a large amount of data to buffer cache cannot fill all the available space since it should block until the current dirty ratio drops below the configured ratio. As a consequence, a foreground task is not blocked by a burst of background writes. Moreover, a foreground task does not need to depend on a large amount of buffered writes generated by background task when it calls \texttt{fsync()} due to the file system ordering requirement~\cite{Yang2008,Yang2015}.

% Block layer: admission
In the admission control stage at the block layer, we separately allocate the block queue slots for critical and non-critical I/Os, respectively, so that the admission control is isolated between critical and non-critical I/Os. To resolve the dependency to the inflight I/O blocked at this stage, we transiently give critical I/O priority to the requesting task recorded in the corresponding \texttt{ncio} object and wake up the task to make it retry allocation of available slot with critical I/O priority. By doing so, the criticality-inherited I/O can avoid the unnecessary congestion with other non-critical I/Os for the admission into the block-level queue. 

%Recall that an I/O dependency can occur to an I/O staged at this admission control stage. This means that a thread issuing a non-critical I/O is blocked waiting for an available queue slot. To promote the non-critical I/O to be critical, the critical I/O priority is transiently inherited to the blocked thread (by an inheriting thread) and the blocked thread is woken up. Then, the awakened thread now has the critical I/O priority and when it retries to allocate a queue slot, it checks queue slots for critical I/Os. By dong so, the criticality-inherited I/O can avoid unnecessary congestion with other non-critical I/Os.  

% Block layer: scheduling
We also designed a simple priority-based I/O scheduler at the block layer. In particular, our scheduler maintains two FIFO queues that are dedicated for critical and non-critical I/Os each. Then, all I/Os in the critical queue is dispatched first before any I/O in the non-critical queue. To prevent starvation of non-critical I/Os, we use a timer to monitor the non-critical queue and guarantee that at least one non-critical I/O is processed per unit of time (10 ms by default). Furthermore, we added queue promotion support into our scheduler for properly handling a dependency to a non-critical I/O staged at the block-level queue. In order to minimize the interference at the device-level, we conservatively limit the number of non-critical I/Os dispatched to a storage device; this number is configurable and we use one by default. This form of limiting is common in practice for improving responsiveness~\cite{CFQ,Losh2015}. Our scheme can be integrated with an existing feature-rich scheduler like Linux CFQ at the expense of additional engineering to support the I/O priority inheritance.
%Differentiated request merging: merging happens only between same criticality.

%Note that a foreground task trying to write can block at the admission control stage in the caching layer when the dirty ratio has been eventually reached the global threshold (e.g., 20\% by default in Linux). In this case, asynchronous writeback I/Os for cleaning buffer cache should be processed quickly. Therefore, our scheduler proactively moves a configurable number of non-critical writes to the critical queue before reaching the high dirty threshold for maintaining the number of dirty pages in buffer cache below the high threshold. 

%\begin{figure}
%\centering
%\epsfig{file=../figures/transitive-dependency.eps, width=2.7in}
%\vspace{-0.1in}
%\caption{\textbf{Handling transitive dependencies.} \textit{A thick line represents a critical path of request execution while dotted lines indicate blocked execution. A box represents the blocking status of the corresponding task. A thick arrow indicates specific actions described in the corresponding text. FG means a foreground task handling a client's requests.}}
%\vspace{-0.2in}
%\label{fig:transitive-dependency}
%\end{figure}

% Dependency tracking
\vspace{-0.1in}
\subsection{Handling Transitive Dependency}\label{sec:Design4}

Transitive dependencies make effective I/O prioritization more challenging. Consider a dependency chain of tasks ($\tau_{1}, \tau_{2}, ..., \tau_{n}$) where each task $\tau_{i} (1 \leq i \leq n-1)$ is blocked due to a task dependency to $\tau_{i+1}$. The last task $\tau_{n}$ can be in one of the three states: runnable, blocked due to I/O dependency, and blocked at the admission control stages. If the last task is runnable or blocked due to the I/O dependency, handling transitive dependency is done by inheriting the critical I/O priority through the dependency chain. 

However, if $\tau_{n}$ is blocked at one of the admission stages, inheriting the critical I/O priority is insufficient because the cause of the blocking should also be resolved. To handle this case, the applied dirty ratio at the caching layer is transiently changed to that of critical I/Os and the blocked task is woken up. At the block layer, similar to the I/O dependency resolution, the blocked task is inherited the critical I/O priority transiently and the task is woken up. Then, the awakened task retries the admission control with the changed I/O priority. 

In order to resolve the transitive dependencies, we record a blocking status into the descriptor of a task when the task is about to be blocked. A blocking status consists of blocking cause and an object to resolve the cause. Blocking cause can be one of task dependency, I/O dependency, and blocking at admission control stage. For the task dependency cause, a corresponding lock or cond object is recorded. For the I/O dependency cause, an I/O descriptor (i.e., block device identifier and sector number) is recorded. No additional object is recorded for the blocking at admission control stage. Based on the recorded blocking status, a foreground task can effectively track and resolve the blocking causes in the transitive dependencies. 

In our experiments, at most four steps of transitive dependency has occurred. % as shown in Figure~\cite{fig:transitive-dependency}. 
In particular, a foreground task is blocked on an inode mutex for file writes. The mutex is held by a background task and the task is also blocked waiting for the signal by a journaling daemon (i.e., jbd2) since the task tries to open a new file system transaction. The journaling daemon is also waiting for the completion of updating journal handle by another background task. The last background task is blocked on the admission control stage of the block layer because the task is issuing a burst of writeback for carrying out delayed allocation.

%Figure~\ref{fig:transitive-dependency} illustrates an example to describe how the transitive dependencies are handled: (1) foreground task T1 attempts to wait on the cond where background task T2 is registered as the helper task. (2) Then, T2 inherits critical I/O priority from T1, and (3) T1 checks T2's blocking status. Since T2 is currently blocked for the lock held by background task T3, (4) T3 also inherits criticality and (5) T1 again inspects T3's blocking status. (6) Finally, T1 wakes up T3 in order to make T3 retry admission with boosted criticality because T3 is blocked waiting for being admitted to either write to buffer cache or enter to block-level queue. As a result, unnecessary delay in request execution can be avoided by properly resolving the transitive dependencies.

%Futex.   The futex() system call provides a method for waiting until a certain condition becomes true.  It is typically used as a blocking construct in the context of shared-memory synchronization.  When using futexes, the majority of the synchronization operations are performed in user space.  A user-space program employs the futex() system call only when it is likely that the program has to block for a longer time until the condition becomes true.  Other futex() operations can be used to wake any processes or threads waiting for a particular condition. Note that no explicit initialization or destruction is necessary to use futexes; the kernel maintains a futex (i.e., the kernel-internal implementation artifact) only while operations such as FUTEX\_WAIT, described below, are being performed on a particular futex word.
%In this paper, we consider the mutex implementation in pthread library since it is most widely used for multi-threaded applications to implement user locks. Since pthread library already provides a way of passing the owner information to the kernel (especially for implementing priority inheritance)~\cite{}, we exploit this support to resolve the user lock-induced dependency. Specifically, pthread library maintains the owner for each mutex and delivers the owner as a parameter to \texttt{sys\_futex()} when the lock is already acquired. 
% For the pthreads library, we implemented this strategy by overriding routines that could potentially cause a thread to block: pthread_mutex and pthread_cond_wait. To override a routine in a dynamically linked application, we use library preloading. That is, at program launch time, HPCToolKit injects a dynamically linked hooking library into an unmodified program's address space. For statically linked programs, compilation remains unchanged, but we require users to adjust thier likn step to invoke a script that adds HPCToolKit's profiling library to a statically linked executable via linkers's special --wrap option. When a monitored application calls one of the overriden routines, control is transferred to the monitored version of the routine. The override then sets a thread-local idleness flag and immediately calss the actual pthreads routine. When the thread enters the lock or condition variable critical section,  the pthreads routine returns to the override, which immediately clears the idleness flag and returns to the monitored application.
% __wait_on_bit: PG_locked(wait_on_page_locked, wait_on_page_locked_killable), PG_writeback(wait_on_page_writeback <= set_page_writeback()), BH_Lock(wait_on_buffer), BH_Shadow (do_get_write_access (=sleep_on_shadow_bh)).
% __wait_on_bit_lock: PG_locked(lock_page, lock_page_killable), BH_Lock(lock_buffer) 
% wait_queue_head_t       j_wait_transaction_locked: Wait queue for waiting for a locked transaction to start committing, or for a barrier lock to be released. helper task: jbd2
% wait_queue_head_t       j_wait_done_commit: Wait queue for waiting for commit to complete. helper task: jbd2
% wait_queue_head_t       j_wait_commit: Wait queue to trigger commit.
% wait_queue_head_t       j_wait_updates: Wait queue to wait for updates to complete. helper tasks: dynamic.
% wait_queue_head_t       j_wait_reserved: Wait queue to wait for reserved buffer credits to drop.
% Task dependency: lock - mutex, rw_semaphore, user lock(futex, sys v sem), cond - j_wait_transaction_locked (helper=jbd2), j_wait_done_commit(helper=jbd2), j_wait_updates(helper=dynamic).
%A critical process can attempt modify metadata buffer, which was marked shadow buffer by jbd2, corresponding to sector 100 for example. In this case, the critical process should wait for the completion of journal I/O. Moreover, jbd2 also tries to write out other journal buffers (e.g., sector 92), it can be blocked at block layer for getting a free request descriptor, thereby making the critical process blocked more.
