\vspace{-0.15in}
\section{Evaluation}\label{sec:Evaluation}

%In this section, we present experimental results that answer the following questions:
%\begin{itemize}
%    \vspace{-0.05in}
 %   \item Does RCP effectively improve request throughput and latency? (Section~\ref{sec:EvaluationResult1})
  %  \vspace{-0.05in}
    %\item What is the performance impact on application-level tuning? (Section~\ref{sec:EvaluationResult2})
    %\vspace{-0.05in} %\item How does each layer affects application performance? (Section~\ref{sec:EvaluationResult3}) %\vspace{-0.05in} %\item What are the I/O types that consist of critical I/Os? (Section~\ref{sec:EvaluationResult4})
    %\vspace{-0.05in}
%\end{itemize}

\vspace{-0.05in}
\subsection{Experimental Setup}\label{sec:EvaluationSetup}

Our prototype was installed on Dell PowerEdge R530, equipped with two hexa-core Intel Xeon E5-2620 2.4GHz processors with hyperthreading enabled and 64 GB RAM; CPU clock frequency is set to the highest level for stable performance measurement. We used a 1 TB Micron MX200 SSD and two 300 GB Seagate SAS HDDs. We used Ubuntu 14.04 with the modified Linux kernel version 3.13 as an OS and ext4 file system mounted with the default options.

We used CFQ as the baseline for our experiments. In addition, we used CFQ-IDLE to prioritize foreground tasks by putting background tasks (e.g., checkpointer) to idle-priority~\cite{ionice}. We also used the split-level schedulers~\cite{Yang2015}, Split-AFQ (SPLIT-A) and Split-Deadline (SPLIT-D), for comparison.

\vspace{-0.1in}
\subsection{PostgreSQL Relational Database}\label{sec:EvaluationResult1}

We used the OLTP-Bench~\cite{Difallah2013} to generate a TPC-C~\cite{TPCC} workload for PostgreSQL. We simulated 50 clients running on a separate machine for 30 minutes. PostgreSQL was configured to have 40\% buffer pool of the size of the initial database and to checkpoint every 30 seconds. For CFQ-IDLE, we put the checkpointer to the idle-priority. For SPLIT-A, we set 0 and 7 I/O priorities to backends and the checkpointer, respectively. For SPLIT-D, we set 5ms and 200ms \texttt{fsync()} deadlines to backends and the checkpointer, respectively; the configurations are dictated from those in \cite{Yang2015}. We report transactions per second and transaction latency as the performance metrics. 

\begin{figure}[t]
\centering
\epsfig{file=../figures/eval-postgres-thput.eps, width=2.8in}
\vspace{-0.1in}
\caption{\textbf{PotsgreSQL throughput.} \textit{}}
\vspace{-0.1in}
\label{fig:eval-postgres-thput}
\end{figure}

\textbf{Request throughput.}  Figure~\ref{fig:eval-postgres-thput} shows transaction throughput averaged over three runs on an SSD with 100 and 600 TPC-C scale factors, which correspond to about 10 GB and 60 GB of initial databases, respectively. CFQ-IDLE does not help to improve application throughput though the major background task (i.e., checkpointer) are put to idle-class priority because CFQ-IDLE prioritizes high-priority (foreground) I/Os only at the block-level scheduler. SPLIT-A improves transaction throughput only for the case of 600 scale factor. This is mainly because SPLIT-A does not consider the I/O priority inversion caused by runtime dependencies between foreground and background tasks. As presented in \cite{Yang2015}, SPLIT-D is effective in improving the performance because it procrastinates the checkpointing task. RCP outperforms CFQ by about 23\% without sacrificing the checkpointing task; see the next paragraph for the detail.
%PostgreSQL splits data files into maximum 1GB size while MongoDB maintains one file per collection (a group of documents). SPLIT-D in Redis cannot throttle much because aggressive writeback conducted by pdflush due to memory pressure.
%launches asynchronous writeback of the file's dirty data that is checkpointed and waits until the amount of dirty data drops to 100 pages

\begin{figure}[t]
\centering
\epsfig{file=../figures/eval-postgres-wal.eps, width=2.8in}
\vspace{-0.1in}
\caption{\textbf{PostgreSQL transaction log size.}}
\vspace{-0.1in}
\label{fig:eval-postgres-wal}
\end{figure}

\textbf{Impact on background task.} To analyze the impact of each scheme on the background task (i.e., checkpointer), we measured the size of transaction logs during the workload execution in the case of 100 scale factor. As shown in Figure~\ref{fig:eval-postgres-wal}, CFQ and CFQ-IDLE complete checkpointing task regularly as intended, thereby bounding the size of transaction logs to 8GB in total. Though SPLIT-A increases the size of transaction logs, it makes transaction logs converge to 12GB. On the other hand, SPLIT-D fails to bound the size of transactions logs since it significantly penalizes the regular checkpoint by delaying every \texttt{fsync()} calls made by the checkpointer until dirty data of the requested file drops to 100 pages. As a consequence, SPLIT-D leads to unbounded use of storage space and unbounded recovery time, which are undesirable in practice~\cite{LSFMM2014b}. RCP completes the regular checkpoint as frequent as CFQ while improving request throughput.

\begin{figure}[t]
\centering
\epsfig{file=../figures/eval-postgres-latency-2hdds.eps,width=2.8in}
\vspace{-0.1in}
\caption{\textbf{PostgreSQL latency.}}
\vspace{-0.1in}
\label{fig:eval-postgres-latency-2hdds}
\end{figure}

\textbf{Request latency.} In order to show the effectiveness of RCP for improving request latency, we ran rate-controlled TPC-C workload (i.e., fixed number of transactions) with 100 scale factor on two SAS HDDs each of which is used for storing database and transaction log, respectively. Figure~\ref{fig:eval-postgres-latency-2hdds} demonstrates a complementary cumulative distribution function (CCDF), and so the point $(x,y)$ indicates that $y$ is the fraction of requests that experience a latency of at least $x$ ms. This form of representation helps visualizing latency tails, as Y-axis labels correspond to the $0^{th}$, $90^{th}$, $99^{th}$ (and so on) percentile latencies. As in the throughput result, CFQ-IDLE does not help to ameliorate latency tail. Moreover, SPLIT-A rather worsens the request latencies mainly because it hinders foreground tasks from fully utilizing the OS buffer cache by scheduling writes at the system-call layer. SPLIT-D also cannot help to cut down the latency tail since it does not address the kernel- and user-level dependencies. RCP is effective for bounding request latency close to 100 ms. Specifically, RCP improves $99.9^{th}$ percentile latency by about 50$\times$ compared to that of CFQ.

\begin{figure}[t]
\centering
\vspace{-0.1in}
\epsfig{file=../figures/eval-postgres-latencytop-2hdds.eps,width=2.85in}	
\vspace{-0.1in}
\caption{\textbf{PostgreSQL system latency breakdown.} \textit{}}
\vspace{-0.2in}
\label{fig:eval-postgres-latencytop-2hdds}
\end{figure}

To further analyze the latency differences, we measured the wait time of foreground tasks (i.e., PostgreSQL backends) using LatencyTOP~\cite{Edge2008}. As shown in Figure~\ref{fig:eval-postgres-latencytop-2hdds}, CFQ and CFQ-IDLE spent significant amount of time blocked at the \texttt{j\_wait\_trasnaction\_locked} cond to wait for starting a new file system transaction. On the other hand, SPLIT-A incurs significant amount of latencies at write system-calls (i.e., \texttt{write\_entry}) and user-level locks (i.e., \texttt{semtimedop}). Though SPLIT-D reduces system latencies, it still suffers from excessive latencies at \texttt{mutex\_lock} and \texttt{bh\_shadow} up to several seconds. RCP mostly eliminates excessive system latencies by effectively prioritizing critical I/Os along the I/O path by handling the kernel- and user-level I/O priority inversions. As a result, only RCP can provide consistent application performance, which is crucial for high degree of user experience.
%In addition, there are many other sources of excessive latencies in the worst case. For instance, \texttt{mutex\_lock} and \texttt{bh\_lock} incur up to about 3.7 and 4.7 seconds. 

\vspace{-0.15in}
\subsection{MongoDB Document Store}\label{sec:EvaluationResult2}

For MongoDB, we used the update-heavy workload (Workload A) in the YCSB~\cite{Cooper2010} benchmark suite. We simulated 150 clients running on a separate machine for 30 minutes. MongoDB was configured to have 40\% internal cache of the size of the initial dataset. We applied the identical configuration to client threads and checkpoint thread as the PostgreSQL case for CFQ-IDLE, SPLIT-A, and SPLIT-D. We report operations per second and operation latency as the performance metrics. 

\begin{figure}[t]
\centering
\vspace{-0.1in}
\epsfig{file=../figures/eval-mongodb-thput.eps, width=2.8in}
%\caption{\textbf{MongoDB throughput.} \textit{10M and 60M objects correspond to about 10GB and 60GB of initial data, respectively.}}
\vspace{-0.1in}
\caption{\textbf{MongoDB throughput.}}
\vspace{-0.1in}
\label{fig:eval-mongodb-thput}
\end{figure}

\textbf{Request throughput.} Figure~\ref{fig:eval-mongodb-thput} plots request throughput averaged over three runs on an SSD with 10 and 60 million objects, which correspond to about 10 GB and 60 GB of initial datasets, respectively. As in the PostgreSQL case, CFQ-IDLE does not help to mitigate the interference from the background task. Moreover,  SPLIT-A decreases the request throughput by up to about 46\% compared to CFQ. This result implies that  blindly prioritizing foreground I/Os without considering I/O priority inversion has its risk for performance regression, and this is why most highly-optimized applications still do not exploit I/O priority already supported in modern OSes. Unlike the case of PostgreSQL, SPLIT-D degrades request throughput rather than improving due to the different application design. MongoDB stores a collection of documents into a single file whereas PostgreSQL splits a database into multiple 1 GB-sized files. Hence, the checkpoint thread in MongoDB writes whole dataset to the collection file and then calls \texttt{fsync()} to the file. In this case, SPLIT-D cannot help to prevent write entanglement since it does not schedule writes at the system-call layer. Meanwhile, scheduling writes at the system-call layer (SPLIT-A) is not also effective because buffered writes are handled slowly as in the PostgreSQL case. On the other hand, RCP effectively improves request throughput by about 10\%--47\%.

\begin{figure}[t]
\centering
\vspace{-0.1in}
\epsfig{file=../figures/eval-mongodb-latency-2hdds.eps,width=2.8in}
\vspace{-0.1in}
\caption{\textbf{MongoDB latency.} \textit{}}
\vspace{-0.1in}
\label{fig:eval-mongodb-latency-2hdds}
\end{figure}

\textbf{Request Latency.} Figure~\ref{fig:eval-mongodb-latency-2hdds} shows CCDF of request latency measured during the execution of the rate-controlled YCSB workload (i.e., fixed number of operations) with 10M objects on two SAS HDDs each of which is used for storing data and log files, respectively. CFQ and CFQ-IDLE significantly increase latency tail especially after $99.9^{th}$ percentile. SPLIT-A and SPLIT-D also cannot help to bound latency tail even in $99.9^{th}$ percentile. RCP reduces $99.9^{th}$ and $99.99^{th}$ percentile latencies by about 2.1$\times$ and 203$\times$, respectively, compared CFQ. Overall, RCP bounds request latency below 300 ms even for $99.999^{th}$ percentile.

\begin{figure}[t]
\centering
\epsfig{file=../figures/eval-mongodb-latencytop-2hdds.eps,width=2.85in}
\vspace{-0.1in}
\caption{\textbf{MongoDB system latency breakdown.} \textit{}}
\vspace{-0.15in}
\label{fig:eval-mongodb-latencytop-2hdds}
\end{figure}

Figure~\ref{fig:eval-mongodb-latencytop-2hdds} shows the wait time breakdown of foreground tasks (i.e., MongoDB client threads). CFQ, CFQ-IDLE, SPLIT-A, and SPLIT-D were blocked mostly at user-level locks/conds, which are shared with the background threads, and at inode mutex. In addition, SPLIT-A and SPLIT-D makes a client thread frequently block at \texttt{nanosleep()}, which is used for spin-then-block spinlock. RCP largely relaxes dependency-induced system latencies, thereby achieving about 2.3$\times$ better average operation latency over CFQ. Note that average operation latency and total system latency does not match because SPLIT-A and SPLIT-D consume significant CPU cycles (over 40\%) for repeatedly yielding a CPU to the log thread as a way of conducting implicit (i.e., non-blocking) synchronization.

\vspace{-0.1in}
\subsection{Redis Key-Value Store}\label{sec:EvaluationResult3}

For Redis, we used the same workload as the MongoDB's except that we concurrently ran ten YCSB benchmarks against ten Redis instances to utilize our multicore testbed due to the single threaded design of Redis~\cite{RedisLatency}. We enabled both snapshotting and command logging for data safety according to the suggestion in the official document~\cite{RedisPersistence}. We report operations per second and operation latency as the performance metrics.

\begin{figure}[t]
\centering
\vspace{-0.1in}
\epsfig{file=../figures/eval-redis-performance.eps, width=2.8in}
\vspace{-0.1in}
\caption{\textbf{Redis performance.} \textit{}}
\vspace{-0.1in}
\label{fig:eval-redis-performance}
\end{figure}

Figure~\ref{fig:eval-redis-performance} plots operation throughput averaged over three runs and $99^{th}$ percentile operation latency. CFQ-IDLE does not help much to improve application performance. SPLIT-A and SPLIT-D deteriorate application performance because SPLIT-A does not fully utilize write buffer in the caching layer and SPLIT-D does not protect non-critical writes from file system-level entanglement. By handling the limitations in the existing prioritization schemes in terms of application performance, RCP improves request throughput by about 10\% and $99^{th}$ percentile request latency by about 8$\times$, compared to CFQ.

\vspace{-0.1in}
\subsection{Need for Holistic Approach}

\begin{figure}[t]
\centering
\epsfig{file=../figures/eval-breakdown.eps, width=2.8in}
\vspace{-0.1in}
\caption{\textbf{Normalized throughput with turning off one of the components.} \textit{}}
\vspace{-0.15in}
\label{fig:eval-breakdown}
\end{figure}

In order to show why a holistic approach is crucial for application performance, we selectively disabled one of the components in our scheme, caching layer, block layer, kernel-level dependency handling, user-level dependency handling, and transitive dependency handling. Figure~\ref{fig:eval-breakdown} shows average request throughput normalized to that all the components are enabled. Disabling any one of the component degrades application performance by about 7--33\% and 6--45\% for PostgreSQL and MongoDB, respectively. This result justifies our claim that only a holistic approach can guarantee high degree of application performance.

%\subsection{Background Task Frequency}\label{sec:EvaluationResult2}

%\subsection{I/O Criticality Analysis}\label{sec:EvaluationResult4}

%Providing for end-to-end tail latency QoS in a shared networked storage system is an important problem in cloud computing environments. Normally, one might look at tail latency at the 90th or 95th percentiles. However, increasingly, researchers and companies like Amazon and Google are starting to care about long tails at the 99th and 99.9th percentiles [4, 5, 24]. This paper addresses tail latencies even at the 99.9th and 99.99th percentiles
