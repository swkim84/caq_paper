\vspace{-0.15in}
\section{Motivation and Related Work}\label{sec:Motivation}
\vspace{-0.1in}
% Interferences from background tasks
%In data-intensive applications, the response time of a request, such as a put/get to a key-value store or a transaction query to a database, determines the level of application performance. Typically, a request is handled by one or more \textit{foreground tasks} in order to deliver the proper response to a client. Due to the importance of request processing latency, I/O-intensive internal activies, such as checkpointing~\cite{Gray1992,PostgresCheckpoint,SQLServerCheckpoint}, backup~\cite{PostgresBackup,MongoBackup,RedisPersistence,Amvrosiadis2015a}, compaction~\cite{Chang2006,Lakshman2010,Bautin2012,PostgresVacuum}, and contents filling~\cite{Losh2015}, are conducted by \textit{background tasks}. Nevertheless, background tasks are still problematic for achieving the high degree of request throughput and latency since they inherently share the I/O path with foreground tasks.

In data-intensive applications, foreground tasks handling requests generate foreground I/Os to reliably store and read data to/from a storage device. Meanwhile, background activities, such as checkpointing, backup, compaction, and contents filling, generate a burst of I/Os periodically or at certain conditions. A burst of I/Os issued by background tasks interfere the request execution thereby degrading the application performance.
%Specifically, we define a \textit{request} or a \textit{request execution} as the set of server activities to service a client call, like a PUT/GET request to a key-value store. 

\begin{figure}[t]
\centering
\epsfig{file=../figures/motiv.eps, width=3in}
\vspace{-0.15in}
\caption{\textbf{Limitation of existing approaches.} \textit{Update-heavy workload in YCSB is executed against MongoDB.}}
\vspace{-0.2in}
\label{fig:motiv}
\end{figure}

% Limitation of existing approach
We illustrate this problem by running the YCSB~\cite{Cooper2010} benchmark against MongoDB~\cite{Mongo} document store on a Linux platform; see Section~\ref{sec:Evaluation} for the details. As shown in Figure~\ref{fig:motiv}, application performance represented as operations per second is highly fluctuated with the CFQ~\cite{CFQ} in Linux due to the checkpointer periodically running in MongoDB (60 seconds by default). A solution to remedy this performance interference would be to prioritize foreground I/Os over background ones. However, simply putting the checkpointer to idle-priority~\cite{ionice} supported by CFQ (CFQ-IDLE) cannot alleviate the performance interference. Moreover, Split-AFQ (SPLIT-A) and Split-Deadline (SPLIT-D), which are the state-of-the-art cross-layer I/O schedulers~\cite{Yang2015}, also cannot guarantee consistent application performance even though the checkpointer is given lower priority than foreground ones. Note that adjusting the parameters in the SPLIT-A/D (e.g., \texttt{fsync()} deadline) did not show any noticeable improvement.
%we set different I/O priorities (0 vs. 7) in SPLIT-A and \texttt{fsync()} deadlines (5ms vs. 200ms) in SPLIT-D for the foreground tasks and the checkpointing task, respectively
 
\begin{figure*}[t]
\centering
\epsfig{file=../figures/overview.eps, width=5.5in}
\vspace{-0.1in}
\caption{\textbf{Challenges in modern storage stacks.} \textit{FG in a circle and a box mean a foreground task and an I/O, respectively. Likewise, BG in a circle and a box indicate a background task and an I/O each.}}
\vspace{-0.2in}
\label{fig:overview}
\end{figure*}

% Root causes
The root causes of this undesirable behavior in the existing I/O prioritization schemes are twofold. First, the existing schemes do not fully consider multiple independent layers including caching, file system, and block layers in the modern storage stacks. Prioritizing I/Os only in one or two layers of the I/O path cannot help much to provide differentiated service for foreground tasks. Second and more importantly, the existing schemes do not address the \textit{I/O priority inversion} problem caused by runtime dependencies among concurrent tasks and I/Os. The I/O priority inversion can occur across different I/O stages in multiple layers due to transitive dependencies. As shown by RCP in Figure~\ref{fig:motiv}, the cliffs in application throughput can be significantly minimized, thereby improving $99.9^{th}$ percentile latency by an order of magnitude compared to that of CFQ (Figure~\ref{fig:eval-mongodb-latency-2hdds}), if we carefully consider the context of request execution during the course of I/O processing. In the rest of this section, we explain the key challenges for effective I/O prioritization in terms of application performance and discuss existing approaches.
\vspace{-0.1in}
\subsection{Multiple Independent Layers}\label{sec:Challenge1}
\vspace{-0.05in}

% Multiple independent layers in storage stack
In modern storage stacks, there are multiple independent layers in the I/O path (Figure~\ref{fig:overview}). A caching layer first serves reads if it has the requested block and it buffers writes until they are issued to a lower layer. If a read miss occurs or a writeback of buffered writes is required, a file system generates block I/O requests and passes them to the block layer. Then, a block layer admits an I/O request into a block-level queue and schedules a queued I/O request to dispatch to a storage device. Finally, a storage device admits an I/O command received from a host into a device-internal queue and schedules a queued command to an internal device controller. Though this form of layering with abstraction is an essential part in computer systems for interoperability between layers and independent innovations within each layer, it makes effective I/O prioritization strenuous because each layer has independent policy based on limited information. 

% Problem on caching layer
In the caching layer, request-context-agnostic, which does not differentiate the context of request execution from background tasks, admission control can harm the application performance. Modern OSes, such as Linux~\cite{Corbet2011} and Windows~\cite{Russinovich2012}, control the admission of buffered writes based on dirty ratio of available memory for ensuring system stability. In this case, a foreground task attempting to write to a buffer cache can block until the dirty ratio drops below a predefined threshold if a background task already dirtied a large amount of available memory. Giving higher I/O priority to the foreground task is ineffective unless the caching layer understands and enforces I/O priority to its throttling mechanism.
%CFQ aims to allocate disk time fairly among processes in proportion to priority ranging from 0 (lowest) to 7 (highest).

% Problem on file system layer
After passing through the admission control at the caching layer, a file system forces specific ordering of writes for consistent updates~\cite{Rosenblum1991,Sweeney1996,Mathur2007}, thereby complicating effective I/O prioritization. For example, ext4 file system, which is the default for most Linux distributions, entangles buffered writes into a single, global, compound transaction that needs to be durable atomically. Since a file system transaction contains dirty pages from any file written by any task, a foreground task calling \texttt{fsync()} should wait for the completion of I/Os issued not only by itself, but also by a background task.
%This compound transaction is written to a storage device by a kernel thread (i.e., \textit{jbd2}) on behalf of the tasks that initially caused updates.

% Problem on block layer
In the block layer, request-context-agnostic admission control and I/O scheduling can also degrade the application performance. Typically, the size of a block-level queue is limited to restrict memory usage and to carry out congestion control~\cite{Yang2008}. In this case, a burst of background I/Os can significantly delay the processing of a foreground I/O by quickly filling the available slots in a block-level queue. The existing priority schedulers cannot help to mitigate this problem because they have no control for the submitted I/Os that are not yet entered to the block-level queues. Moreover, a foreground I/O can suffer from excessive delay while staging at a block-level queue if an I/O scheduler does not differentiate the foreground I/O from a burst of background I/Os.

% Problem on device layer
Even after a foreground I/O becomes ready to dispatch to a storage device, the processing of the foreground I/O can be further prolonged. This is because the size of a device-internal queue (e.g., NCQ~\cite{Intel2003}) is also limited and a device firmware reorders I/O commands based on the internal geometry of storage media for improving device throughput~\cite{Yu2010,Filip2013}. Hence, a foreground I/O can be staged before entering a device-internal queue when background I/Os already filled the available queue slots. Furthermore, a foreground I/O can be put behind a background I/O dispatched later by a block-level scheduler depending on the internal scheduling policy.
%Since disabling the device-level queueing degrades the overall throughput for foreground I/Os, we need more intelligent way to properly prioritize foreground I/Os in the existence of the device-level queueing.

\vspace{-0.1in}
\subsection{I/O Priority Inversion}\label{sec:Challenge2}

% I/O priority inversion problem
The most straightforward way for improving application performance in the existence of background tasks would be to prioritize foreground I/Os over background ones. This simple prioritization, however, does not work effectively even if each layer in the I/O path respect priority since background I/Os also can directly/indirectly delay the progress of a foreground task (i.e., request execution) due to the I/O priority inversion caused by runtime dependencies (Figure~\ref{fig:overview}). Similar to the priority inversion in CPU scheduling~\cite{Lampson1980}, the I/O priority inversion is problematic because the processing of a background I/O on which a foreground task depend can be arbitrarily delayed by concurrent background I/Os.

% Causes of I/O priority inversion: task dependency
Two types of dependencies cause the I/O priority inversion: a \textit{task dependency} and an \textit{I/O dependency}. The task dependency occurs when two tasks interact with each other via synchronization primitives, such as a lock and a cond (i.e., condition variable). The dependency caused by a lock complicates effective I/O prioritization because a background task can be blocked waiting for an I/O within a critical section that a foreground task needs to enter. For instance, a foreground task attempting to write a file can be blocked on an inode mutex if the mutex is already held by a background task concurrently writing to the different part of that file. Likewise, the dependency caused by a cond is also problematic since a foreground task should indirectly wait for the I/Os awaited by a background task that is going to signal the foreground task. For example, a foreground task should wait on a specific cond signaled by a kernel thread (e.g., jbd2 in Linux) that processes file system transaction when the task calls \texttt{fsync()}.

% Causes of I/O priority inversion: I/O dependency
Meanwhile, the I/O dependency occurs between a task and an outstanding I/O issued by another task. Basically, the I/O dependency is generated when a task needs to directly wait for the completion of an ongoing I/O in order to ensure correctness and/or durability. For example, when a foreground task calls \texttt{fsync()}, it blocks on the completion of a write I/O that is asynchronously issued by a kernel thread (e.g., pdflush in Linux) for cleaning buffer cache. Once task and I/O dependency-induced priority inversions occur, the foreground task should wait for a long time because each layer in the I/O path can arbitrarily prolong the processing of low-priority background I/Os.
%Likewise, a foreground task blocks on the completion of an ongoing read I/O that is issued by a background task conducting backup or compaction when the foreground task attempts to read the block that is being read into memory by the background task. 
 
% Difficulties of dependency resolution
Unfortunately, resolving the I/O priority inversion is challenging for the following reasons. Firstly, dependency relationships cannot be statically determined since they depend on various runtime conditions, such as execution timing, resource constraint, and client requirement. For example, a foreground task do not always depend on the progress of a kernel thread handling file system transaction since the kernel thread periodically writes out transactions in background~\cite{ext4}. Secondly, dependency occurs in a transitive manner involving multiple concurrent tasks blocked at either synchronization primitives or various I/O stages in multiple layers. We empirically found that a dependency sometimes cascaded in four steps due to the complex interaction between delayed allocation and crash-consistency mechanism in a file system (Section~\ref{sec:Design4}). Finally, a dependency relationship might not be visible at the kernel-level because of the extensive use of user-level synchronizations (e.g., shared memory mutex) based on kernel-level supports (e.g., Futex~\cite{futex} and System V Semaphore~\cite{semop}) in modern applications.

\begin{table}[t]
\begin{center}
\begin{footnotesize}
\begin{tabular}{|c||c|c|c|c|c|}
\hline
\multirow{3}{*}{\textbf{Scheme}} & \multicolumn{3}{|c|}{\textbf{Multiple}} & \multicolumn{2}{|c|}{\textbf{I/O Priority}} \\
& \multicolumn{3}{|c|}{\textbf{Independent Layers}} & \multicolumn{2}{|c|}{\textbf{Inversion}} \\
\cline{2-6}	
& Caching & Filesystem & Block & Kernel & User \\
\hline
\textbf{CFQ~\cite{CFQ}}& No & No & Yes & No & No \\
%\hline
%\hline
\textbf{Redline~\cite{Yang2008}} & Yes & Yes & Yes & No & No \\
\textbf{Split~\cite{Yang2015}} & Yes & Yes & Yes & No & No \\
\textbf{QASIO~\cite{Jeong2015}} & No & No & Yes & Yes & No \\
%\hline
\textbf{sCache~\cite{Kim2015}} & No & No & No & Yes & No \\
%\hline
\textbf{RCP} & Yes & Yes & Yes & Yes & Yes \\
\hline
\end{tabular}
\end{footnotesize}
\end{center}
\vspace{-0.2in}
\caption{\textbf{I/O prioritization challenges.} \textit{This table shows whether a specific challenge for effective I/O prioritization is addressed or not in each previous work.}}
\vspace{-0.2in}
\label{tab:challenges}
\end{table}

\vspace{-0.1in}
\subsection{Related Work}\label{sec:RelatedWork}

Table~\ref{tab:challenges} summarizes how different challenges are addressed (or not) by the existing prioritization schemes. CFQ~\cite{CFQ} is a block-level I/O scheduler that supports multiple priority classes (real-time, best-effort, and idle) and priority levels (0 to 7)~\cite{ionice}. However, CFQ is limited by its inability to effectively prioritize high-priority tasks over low-priority ones because CFQ solely focuses on enforcing I/O priority at the block-level queue and it also does not consider the I/O priority inversion problem.
%Among the priority classes, the idle-priority would be suitable for a background task since it only gives a disk time to the background task when no other task has asked for disk I/O. 

Redline~\cite{Yang2008} adapts all the layers in the I/O path in order to limit the interference from background tasks (e.g., virus scanner) for improving responsiveness of interactive applications (e.g., web browser). Redline, however, does not consider the I/O priority inversion problem that occurs between foreground and background tasks in typical data-intensive applications.

Recently, Split~\cite{Yang2015} addresses the inability exist in the block-level schedulers, such as CFQ, by introducing the cross-layer I/O scheduling framework. Basically, Split provides additional hooks to multiple layers for supporting correct cause mapping, cost estimation, and reordering, in the existence of the file system challenges like delayed allocation and journaling~\cite{Yang2015}. Based on the proposed framework, Split-AFQ and Split-Deadline have been implemented to prove its effectiveness. Split-AFQ is a priority-based scheduler designed for handling the limitations in CFQ. Split-AFQ basically schedules write I/Os including \texttt{write()} and \texttt{fsync()} at the system-call layer based on the given I/O priority (0 to 7) to a task, before a file system entangles them into a single transaction. Split-Deadline is a cross-layer version of the Deadline I/O scheduler~\cite{Deadline} that schedules I/Os based on the preconfigured deadlines to reads and writes, respectively. In Split-Deadline, different \texttt{fsync()} deadlines can be given to each task while reads and writes are handled at the block-level as in the Deadline. In addition, if a pending \texttt{fsync()} may affect other tasks by causing too much I/O, Split-Deadline launches asynchronous writeback of the requested file's dirty data and waits until the amount of dirty data drops to a specific point. Though Split provides generic cross-layer framework that can be used as a basis for prioritizing foreground tasks, it does not specifically consider the I/O priority inversion problem to the framework design despite its significance for effective I/O prioritization.

On the other side, QASIO~\cite{Jeong2015} considers the I/O priority inversion for improving system responsiveness. However, QASIO solely focuses on the kernel-level dependencies to asynchronous writes based on the analysis of the several mobile app scenarios. Likewise, sCache~\cite{Kim2015} fully considers the I/O priority inversion at the kernel-level in order to effectively utilize non-volatile write caches. Both QASIO and sCache, however, do not consider the I/O priority inversion at the user-level. Moreover, they do not specifically address the challenge in the I/O path for effective I/O prioritization.

Though several challenges for effective I/O prioritization have been separately addressed in the previous work, we argue that only a holistic approach can deliver consistently high application performance as shown in Figure~\ref{fig:motiv}. This is because the I/O priority inversion problem can be worsened when combined with multiple layers as a dependency transitively occurs across layers. Our scheme (RCP) addresses all the challenges in Table~\ref{tab:challenges} by enlightening the I/O path considering the kernel- and user-level I/O priority inversions.

% IOFlow. Complementary work. We have investigated IOFlow within a single instance.
