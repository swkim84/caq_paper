\vspace{-0.1in}
\section{Evaluation}\label{sec:Evaluation}
\vspace{-0.1in}

\subsection{Experimental Setup}\label{sec:EvaluationSetup}
\vspace{-0.05in}

The evaluation system is a Dell PowerEdge R530 server that is equipped with two Intel Xeon E5-2620 processors and 64 GB RAM. The CPU frequency is set to the highest level and hyper-threading is enabled. A single 1 TB Micron MX200 SSD is used to store data sets for the evaluation workloads. We used Ubuntu 14.04 with the modified Linux kernel version 3.13 as an OS and ext4 file system mounted with the default options.

We used CFQ as the baseline for our experiments. In addition, we used CFQ-IDLE to prioritize foreground tasks by putting background tasks (e.g., checkpointer) to idle-priority~\cite{ionice}. We also used the split-level schedulers~\cite{Yang2015}, including Split-AFQ (SPLIT-A) and Split-Deadline (SPLIT-D), and QASIO~\cite{Jeong2015} for comparison.

For RCP, we configured 1\% dirty ratio for non-critical writes and 20\% dirty ratio (the default ratio in Linux) for critical writes. We separately allocated 128 slots for each critical and non-critical block-level queues. The number of non-critical I/Os outstanding to a storage device is limited to one, and the timeout for non-critical I/Os is set to 10 ms to prevent starvation at the block-level queue.

\begin{figure}[t]
\centering
\vspace{-0.1in}
\epsfig{file=../figures/eval-postgres-thput.eps, width=2.8in}
\vspace{-0.1in}
\caption{\textbf{PotsgreSQL request throughput.} \textit{}}
\vspace{-0.15in}
\label{fig:eval-postgres-thput}
\end{figure}

\vspace{-0.1in}
\subsection{PostgreSQL Relational Database}\label{sec:EvaluationResult1}
\vspace{-0.05in}

We used the OLTP-Bench~\cite{Difallah2013} to generate a TPC-C~\cite{TPCC} workload for PostgreSQL. We simulated 50 clients running on a separate machine for 30 minutes. PostgreSQL was configured to have 40\% buffer pool of the size of the initial database and to checkpoint every 30 seconds. For CFQ-IDLE and QASIO, we put the checkpointer to the idle-priority. For SPLIT-A, we set the highest and the lowest I/O priorities to backends and the checkpointer, respectively. For SPLIT-D, we set 5 ms and 200 ms \texttt{fsync()} deadlines to backends and the checkpointer, respectively; the configurations are dictated from those in \cite{Yang2015}. We report transactions per second and transaction latency as the performance metrics.

\textbf{Request throughput.}  Figure~\ref{fig:eval-postgres-thput} shows transaction throughput averaged over three runs on an SSD with 100, 600, and 2000 TPC-C scale factors, which correspond to about 10 GB, 60 GB, and 200 GB of initial databases, respectively. We used unlimited request rate (i.e., zero idle/think time) for this experiment. CFQ-IDLE does not help to improve application throughput though we put the major background task (i.e., checkpointer) to idle-class priority because CFQ-IDLE prioritizes high-priority (foreground) I/Os only at the block-level scheduler. SPLIT-A improves transaction throughput only when read I/Os are dominant as scale factor increases. This is because SPLIT-A does not consider the I/O priority inversion problem and hinders foreground tasks from fully utilizing the OS buffer cache by scheduling writes at the system-call layer. As presented in \cite{Yang2015}, SPLIT-D is effective in improving the PostgreSQL's performance mainly because it procrastinates the checkpointing task. Though QASIO addresses I/O priority inversions induced by kernel-level dependencies, QASIO does not show any noticeable improvement due to the unresolved dependencies and the inability existed in the block-level scheduler as in CFQ-IDLE. RCP outperforms the existing schemes by about 15\%--37\%, 8\%--28\%, and 6\%--31\% for 100, 600, and 2000 scale factors, respectively.

\begin{figure}[t]
\centering
\vspace{-0.1in}
\epsfig{file=../figures/eval-postgres-wal.eps, width=2.8in}
\vspace{-0.1in}
\caption{\textbf{PostgreSQL transaction log size.}}
\vspace{-0.1in}
\label{fig:eval-postgres-wal}
\end{figure}

\textbf{Impact on background task.} To analyze the impact of each scheme on the background task (i.e., checkpointer), we measured the size of transaction logs during the workload execution in the case of 100 scale factor. As shown in Figure~\ref{fig:eval-postgres-wal}, CFQ, CFQ-IDLE, and QASIO complete checkpointing task regularly as intended, thereby bounding the size of transaction logs to 8GB in total. SPLIT-A increases the size of transaction logs to 12GB. On the other hand, SPLIT-D increases the size of transaction logs by 3.6$\times$ over CFQ since it penalizes the regular checkpoint by delaying every \texttt{fsync()} calls made by the checkpointer until the dirty data of the requested file drops to 100 pages. As a result, SPLIT-D leads to using more storage space and possibly taking longer recovery time, which are undesirable in practice~\cite{LSFMM2014b}. RCP completes the checkpointing task as frequent as CFQ while improving request throughput. Note that the log sizes with the other scale factors show similar trend.

\begin{table*}[t]
\begin{center}
\vspace{-0.1in}
\begin{footnotesize}
\begin{tabular}{|c||c|c|c|c|c|c|}
\hline
Latency (ms) & \textbf{CFQ} & \textbf{CFQ-IDLE} & \textbf{SPLIT-A} & \textbf{SPLIT-D} & \textbf{QASIO} & \textbf{RCP} \\
\hline
\textbf{semtimedop} & 1351.8 (3.9) & 3723.0 (4.2) & 3504.4 (3.4) & 1951.1 (3.4) & 2241.7 (2.7) & 247.8 (1.0) \\
\hline
\textbf{j\_wait\_done\_commit} & 1282.9 (55.9) & 1450.1 (66.0) & 342.4 (6.4) & 1886.1 (10.6) & 198.0 (3.8) & 23.9 (2.5) \\
\hline
\textbf{PG\_writeback} & 490.2 (0.2) & 1677.6 (0.1)	& 454 (0.2) & 458.0 (0.2)	& 454.7 (0.1) & 243.2 (0.1) \\
\hline
\textbf{get\_request} &	481.8 (3.0) & 3722.8 (22.0)	& 405.2 (1.3) & 240.1 (2.8) & 2241.1 (3.7) & 1.3 (0.1) \\
\hline
\textbf{j\_wait\_transaction\_locked} & 306.5 (68.8) & 229.4 (53.2) & 0.4 (0.1) & 2.4 (0.2) & 0.3 (0.1) & 0.2 (0.1) \\
\hline
\textbf{BH\_Lock} & 201.3 (40.3) & 1339.7 (356.7) & 1.1 (0.1) & 53.9 (11.1) & 0.0 (0.0) & 1.7 (0.5) \\
\hline
\textbf{rwsem\_down} & 92.4 (8.9) & 357.1 (179.7) & 0.8 (0.1) & 33.4 (1.4) & 0.0 (0.0) & 2.3 (0.2) \\
\hline
\textbf{BH\_Shadow} & 46.5 (2.9) & 15.9 (2.9) & 208.9 (3.8) & 236.1 (14.1) & 1294.0 (36.9) & 2.4 (0.2) \\
\hline
\textbf{mutex\_lock} & 32.7 (7.0) & 16.3 (3.1) & 18.6 (2.5) & 944.3 (53.3) & 53.3 (3.1) & 0.4 (0.1) \\
\hline
\textbf{write\_entry} & N/A & N/A & 1703.2 (1.8) & 0.0 (0.0) & N/A & N/A \\
\hline
\textbf{fsync\_entry} & N/A & N/A & 1084.4 (0.5) & 0.0 (0.0) & N/A & N/A \\
\hline
\end{tabular}
\end{footnotesize}
\end{center}
\vspace{-0.2in}
\caption{\textbf{PostgreSQL system latency breakdown.} \textit{This table shows the maximum latency incurred at each kernel method in milliseconds; the corresponding average latency is presented in parenthesis.}}
\vspace{-0.15in}
\label{tab:eval-postgres-latencytop}
\end{table*}

\textbf{Request latency.} In order to show the effectiveness of RCP in terms of request latency, we ran rate-controlled TPC-C workload (i.e., fixed number of transactions) with 100 scale factor. Figure~\ref{fig:eval-postgres-latency} demonstrates a complementary cumulative distribution function (CCDF), and so the point $(x,y)$ indicates that $y$ is the fraction of requests that experience a latency of at least $x$ ms. This form of representation helps visualizing latency tails, as y-axis labels correspond to the $0^{th}$, $90^{th}$, $99^{th}$ (and so on) percentile latencies. Though CFQ-IDLE, SPLIT-D, and QASIO achieves better latency than CFQ, all the existing schemes induce several seconds request latency after $99^{th}$ percentile. This is because the critical path of request execution is arbitrarily prolonged at the various stages in the I/O path. On the other hand, RCP bounds request latency up to around 300 ms. We omit the latency results with 600 and 2000 scale factors because it is similar to that with 100 scale factor.

\begin{figure}[t]
\centering
\epsfig{file=../figures/eval-postgres-latency.eps,width=2.8in}
\vspace{-0.1in}
\caption{\textbf{PostgreSQL request latency.}}
\vspace{-0.15in}
\label{fig:eval-postgres-latency}
\end{figure}

To analyze the reason behind the performance differences, we measured the maximum and average wait time of foreground tasks (i.e., PostgreSQL backends) in kernel functions using LatencyTOP~\cite{Edge2008}. As shown in Table~\ref{tab:eval-postgres-latencytop}, CFQ incurs tens to a thousand milliseconds latency at various synchronization methods. In particular, foreground tasks suffer from excessive latency at SysV sem (\texttt{semtimedop}) and \texttt{j\_wait\_done\_commit} condition variable. CFQ-IDLE additionally causes several seconds latency waiting for the allocation of a block-level queue slot (\texttt{get\_request}), the writeback of cache pages (\texttt{PG\_writeback}), and the acquisition of a buffer lock (\texttt{BH\_Lock}). SPLIT-A and -D are effective in resolving the file system-induced dependencies by scheduling writes at the system-call layer. However, SPLIT-A causes over one second latency at \texttt{write\_entry} and \texttt{fsync\_entry} because it prevents foreground tasks from fully utilizing the OS buffer cache. SPLIT-D also causes about one second latency at inode mutex (\texttt{mutex\_lock}) due to the I/O priority inversion. Though QASIO resolves some dependency-induced latencies ocurred in CFQ-IDLE, it still incurs excessive latencies at \texttt{semtimedop}, \texttt{get\_request}, and \texttt{BH\_Shadow}. On the contrary, RCP bounds all the latencies below 250 ms.

\vspace{-0.1in}
\subsection{MongoDB Document Store}\label{sec:EvaluationResult2}
\vspace{-0.05in}

\begin{figure}[t]
\centering
%\vspace{-0.1in}
\epsfig{file=../figures/eval-mongodb-thput.eps, width=2.8in}
\vspace{-0.1in}
\caption{\textbf{MongoDB request throughput.}}
\vspace{-0.15in}
\label{fig:eval-mongodb-thput}
\end{figure}

For MongoDB, we used the update-heavy workload (Workload A) in the YCSB~\cite{Cooper2010} benchmark suite. We simulated 150 clients running on a separate machine for 30 minutes. MongoDB was configured to have internal cache of size 40\% of the initial data set. We applied the identical configuration to client threads and checkpoint thread as the PostgreSQL case for CFQ-IDLE, SPLIT-A, SPLIT-D, and QASIO. We report operations per second and operation latency as the performance metrics. 

\begin{table*}[t]
\begin{center}
\vspace{-0.1in}
\begin{footnotesize}
\begin{tabular}{|c||c|c|c|c|c|c|}
\hline
Latency (ms) & \textbf{CFQ} & \textbf{CFQ-IDLE} & \textbf{SPLIT-A} & \textbf{SPLIT-D} & \textbf{QASIO} & \textbf{RCP} \\
\hline
\hline
\textbf{futex\_lock\_pi} & 6092.6 (3.3) & 11849.3 (3.4) & 8300.6 (3.6) & 12292.6 (3.7) & 3717.4 (3.1) & 305.6 (3.2) \\
\hline
\textbf{j\_wait\_done\_commit} & 6067.3 (2.3) & 11846.2 (2.4) & 4066.9 (2.4) & 10598.4 (2.6) & 3652.1 (2.0) & 246.5 (2.0) \\
\hline
\textbf{pg\_writeback} & 239.5 (0.1) & 240.1 (0.1) & 426.2 (0.2) & 241.0 (0.2) & 241.8 (0.2) & 64.2 (0.1) \\
\hline
\textbf{get\_request} &	35.0 (18.4) & 48636 (26.6) & 17.3 (5.5) & 0.0 (0.0) & 852.5 (6.8) & 0.0 (0.0) \\
\hline
\textbf{j\_wait\_transaction\_locked} & 2.2 (1.0) & 1790.3 (44.1) & 1.4 (0.1) & 0.0 (0.0) & 1942.6 (24.0) & 2.0 (0.9) \\
\hline
\textbf{mutex\_lock} & 0.0 (0.0) & 3296.6 (544.2) & 0 (0.0) & 1.2 (0.1) & 992.0 (77.2) & 0.0 (0.0) \\
\hline
\textbf{write\_entry} & N/A & N/A & 7884.9 (27.4) & 0.0 (0.0) & N/A & N/A \\
\hline
\textbf{fsync\_entry} & N/A & N/A & 8273.1 (26.2) & 0.0 (0.0) & N/A & N/A \\
\hline
\end{tabular}
\end{footnotesize}
\end{center}
\vspace{-0.2in}
\caption{\textbf{MongoDB system latency breakdown.} \textit{This table shows the maximum latency incurred at each kernel method in milliseconds; the corresponding average latency is presented in parenthesis.}}
\vspace{-0.2in}
\label{tab:eval-mongodb-latencytop}
\end{table*}
\textbf{Request throughput.} Figure~\ref{fig:eval-mongodb-thput} plots request throughput averaged over three runs with 10, 60, and 200 million objects, which correspond to about 10 GB, 60 GB, and 200 GB of initial data sets, respectively. As in the PostgreSQL case, CFQ-IDLE and QASIO do not help to mitigate the interference from background tasks. Unlike the case of PostgreSQL, SPLIT-D degrades request throughput rather than improving due to the different application design. MongoDB stores a collection of documents into a single file whereas PostgreSQL splits a database into multiple 1 GB-sized files. Hence, the checkpoint thread in MongoDB writes whole data set to the collection file and then calls \texttt{fsync()} to the file. In this case, SPLIT-D cannot help to prevent write entanglement since it does not schedule writes at the system-call layer. Meanwhile, scheduling writes at the system-call layer (SPLIT-A) is not also effective because buffered writes are handled slowly as in the PostgreSQL case. On the other hand, RCP improves request throughput by about 53\%--152\%, 12\%--136\%, and 12\%--201\% for 10, 60, and 200 million objects, respectively, compared to the existing schemes.

\begin{figure}[t]
\centering
%\vspace{-0.1in}
\epsfig{file=../figures/eval-mongodb-latency.eps,width=2.8in}
\vspace{-0.1in}
\caption{\textbf{MongoDB request latency.} \textit{}}
\vspace{-0.15in}
\label{fig:eval-mongodb-latency}
\end{figure}

\textbf{Request latency.} Figure~\ref{fig:eval-mongodb-latency} shows CCDF of request latency measured during the execution of the rate-controlled YCSB workload (i.e., fixed number of operations) with 10 million objects. CFQ and CFQ-IDLE significantly increase latency tail especially after $99^{th}$ percentile. SPLIT-A and SPLIT-D also cannot help to bound latency; about five seconds latency at $99.99^{th}$ percentile. Though QASIO improves request latency compared to the other existing schemes, it still incurs about four seconds latency at $99.999^{th}$ percentile. On the contrary, RCP completes all the requests within 310 ms by carefully handling the critical path of request execution. Note that the latency results with 60 and 200 million objects is similar to that of 10 million objects.

Table~\ref{tab:eval-mongodb-latencytop} shows the maximum and average system latencies in foreground tasks. All the existing schemes induce excessive latencies at various synchronization methods, such as Pthread mutex (\texttt{futex\_lock\_pi}) and \texttt{j\_wait\_done\_commit} condition variable. Unlike the case of PostgreSQL, CFQ-IDLE and QASIO cause over a second latency at \texttt{j\_wait\_transaction\_locked} condition variable, which is a barrier for starting a new file system transaction. RCP largely reduces dependency-induced system latencies
.
\vspace{-0.1in}
\subsection{Redis Key-Value Store}\label{sec:EvaluationResult3}
\vspace{-0.05in}

For Redis, we used the same workload as the MongoDB's except that we concurrently ran ten YCSB benchmarks against ten Redis instances to utilize our multicore testbed due to the single threaded design of Redis~\cite{RedisLatency}. We enabled both snapshotting and command logging for data safety~\cite{RedisPersistence}. We report operations per second and operation latency as the performance metrics.

\begin{figure}[t]
\centering
%\vspace{-0.1in}
\epsfig{file=../figures/eval-redis-performance.eps, width=2.8in}
\vspace{-0.1in}
\caption{\textbf{Redis performance.} \textit{}}
\vspace{-0.15in}
\label{fig:eval-redis-performance}
\end{figure}

Figure~\ref{fig:eval-redis-performance} plots operation throughput averaged over three runs and $99.9^{th}$ percentile operation latency. CFQ-IDLE and QASIO slightly improves application performance by putting the background tasks including the snapshotting and log rewriting to idle-class priority. SPLIT-A and SPLIT-D deteriorate application performance because SPLIT-A does not fully utilize write buffer in the caching layer and SPLIT-D does not protect non-critical writes from file system-level entanglement. By handling the limitations in the existing prioritization schemes in terms of application performance, RCP improves request throughput by about 7\%--49\% compared to the existing schemes. In addition, RCP shows 78 ms request latency at $99.9^{th}$ percentile, thereby achieving 2$\times$--20$\times$ improvement over the existing schemes.

\vspace{-0.1in}
\subsection{Need for Holistic Approach}
\vspace{-0.05in}

In order to show why a holistic approach is crucial for application performance, we selectively disabled one of the components in our scheme, the caching layer, the block layer, the kernel-level dependency handling, the user-level dependency handling, and the transitive dependency handling. Figure~\ref{fig:eval-breakdown} shows average request throughput normalized to that of all the components are enabled in the 10 GB data set configurations. Disabling any one of the component degrades application throughput by about 7--33\% and 6--45\% for PostgreSQL and MongoDB each. This result justifies our claim that only a holistic approach can guarantee high degree of application performance.

\begin{figure}[t]
\centering
\vspace{-0.1in}
\epsfig{file=../figures/eval-breakdown.eps, width=2.8in}
\vspace{-0.1in}
\caption{\textbf{Normalized request throughput with turning off one of the components in RCP.} \textit{}}
\vspace{-0.15in}
\label{fig:eval-breakdown}
\end{figure}

\vspace{-0.1in}
\subsection{Impact of Limiting I/Os}
\vspace{-0.05in}

Due to hidden and unpredictable I/O scheduling inside storage, we limited the number of sojourn non-critical I/Os to one. This may lead to low utilization of storage devices when there is low foreground activity. To quantify the impact on system throughput, we concurrently ran a sequential write workload with 5 ms thinktime as a lightly-loaded foreground task and a random write workload without thinktime as a I/O-intensive background task. As shown in Figure~\ref{fig:eval-fio-performance}, the system-wide throughput increases from 223 MB/sec to 345 MB/sec (55\% improvement) while relaxing the limitation on non-critical I/Os. However, the improvement in system throughput comes with the degraded latency of foreground I/O. In particular, the average latency of foreground I/O gradually increases from 110 us to 1771 us (over 16$\times$ slowdown). If storage devices implement priority-based scheduling feature in storage interface standards (e.g., SCSI, ATA, and NVMe), this tradeoff would be minimized by exploiting the priority feature.
