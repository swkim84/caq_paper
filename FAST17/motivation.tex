\vspace{-0.1in}
\section{Motivation and Related Work}\label{sec:Motivation}
\vspace{-0.1in}

% Limitation of existing approach
Background I/O-intensive tasks, such as checkpointing and compaction, are problematic for achieving the high degree of application performance. We illustrate this problem by running the YCSB~\cite{Cooper2010} benchmark against MongoDB~\cite{Mongo} document store on a Linux platform with two HDDs each of which is allocated for data and journal, respectively; see Section~\ref{sec:Evaluation} for the details. As shown in Figure~\ref{fig:motiv}, application performance represented as operations per second is highly fluctuated with the CFQ~\cite{CFQ}, the default I/O scheduler in Linux, mainly due to the contention incurred by periodic checkpointing (60 seconds by default)~\footnote{The interference is not exactly periodic because the checkpointing occurs 60 seconds after the completion of the previous checkpointing.}. Assigning low priority (idle-priority~\cite{ionice} in CFQ) to the checkpoint task using the existing interface, denoted as CFQ-IDLE, is also ineffective in alleviating the performance interference. Moreover, Split-AFQ (SPLIT-A) and Split-Deadline (SPLIT-D), the state-of-the-art cross-layer I/O schedulers~\cite{Yang2015}, also cannot provide consistent application performance even though the checkpoint thread is given lower priority than foreground ones; adjusting the parameters in the SPLIT-A/D (e.g., \texttt{fsync()} deadline) did not show any noticeable improvement. Likewise, QASIO~\cite{Jeong2015}, which tries to eliminate I/O priority inversions, also shows frequent drops in application performance. 
 
\begin{figure}[t]
\centering
\vspace{-0.1in}
\epsfig{file=../figures/motiv.eps, width=3in}
\vspace{-0.1in}
\caption{\textbf{Limitation of existing approaches.} \textit{Update-heavy workload in YCSB is executed against MongoDB.}}
\vspace{-0.15in}
\label{fig:motiv}
\end{figure}
 
\begin{figure*}[t]
\centering
\vspace{-0.1in}
\epsfig{file=../figures/overview.eps, width=5.5in}
\vspace{-0.1in}
\caption{\textbf{Challenges in modern storage stacks.} \textit{FG in a circle and a box means a foreground task and an I/O, respectively. Likewise, BG in a circle and a box indicates a background task and an I/O each.}}
\vspace{-0.15in}
\label{fig:overview}
\end{figure*}

% Root causes
The root causes of this undesirable result in the existing I/O prioritization schemes are twofold. First, the existing schemes do not fully consider multiple independent layers including caching, file system, and block layers in modern storage stacks. Prioritizing I/Os only in one or two layers of the I/O path cannot achieve proper I/O prioritization for foreground tasks. Second and more importantly, the existing schemes do not address the \textit{I/O priority inversion} problem caused by runtime dependencies among concurrent tasks and I/Os. I/O priority inversions can occur across different I/O stages in multiple layers due to transitive dependencies. As shown by RCP in Figure~\ref{fig:motiv}, the cliffs in application throughput can be significantly mitigated if the two challenges are addressed. In the rest of this section, we detail the two challenges from the perspective of application performance and discuss existing approaches.

\vspace{-0.1in}
\subsection{Multiple Independent Layers}\label{sec:Challenge1}
\vspace{-0.05in}

% Multiple independent layers in storage stack
In modern OSes, a storage I/O stack is comprised of multiple and independent layers (Figure~\ref{fig:overview}). A caching layer first serves reads if it has the requested block and it buffers writes until they are issued to a lower layer. If a read miss occurs or a writeback of buffered writes is required, a file system generates block I/O requests and passes them to a block layer. Then, the block layer admits an I/O request into a block-level queue and schedules a queued I/O request to dispatch to a storage device. Finally, a storage device admits an I/O command received from a host into a device-internal queue and schedules a queued command to an internal device controller. Though this form of layering with abstraction is an essential part in computer systems for interoperability and independent innovations across the layers, it makes effective I/O prioritization strenuous because each layer has independent policy with limited information. 

% Problem on caching layer
In the caching layer, priority-agnostic admission control can harm the application performance. Modern OSes, such as Linux~\cite{Corbet2011} and Windows~\cite{Russinovich2012}, control the admission of buffered writes based on the dirty ratio of available memory for ensuring system stability. However, since a single system-wide dirty ratio is applied to all tasks, a foreground task can be blocked even if most dirty pages are made by background tasks. Giving higher I/O priority to the foreground task is ineffective unless I/O priority is applied to the dirty throttling.

% Problem on file system layer
A file system forces specific ordering of writes for consistent updates~\cite{Rosenblum1991,Sweeney1996,Mathur2007}, thereby complicating effective I/O prioritization. For example, ext4 file system, which is the default for most Linux distributions, entangles buffered writes into a single, global, compound transaction that needs to be durable atomically. Since a file system transaction contains dirty pages from any file written by any task, a foreground task calling \texttt{fsync()} should wait for the completion of I/Os issued not only by itself, but also by a background task.

% Problem on block layer
In the block layer, many modern block I/O schedulers already reflect the priority of I/Os in their scheduling. However, priority-agnostic admission control can also degrade the application performance. Typically, the size of a block-level queue is limited to restrict memory usage and to control disk congestion~\cite{Yang2008}. In this case, a burst of background I/Os can significantly delay the processing of a foreground I/O by quickly filling the available slots in a block-level queue. The existing priority schedulers cannot help to mitigate this problem because they have no control of submitted I/Os that are not yet entered the block-level queues.

% Problem on device layer
Even after a foreground I/O becomes ready to dispatch to a storage device, the processing of the foreground I/O can be further prolonged. This is because the size of a device-internal queue (e.g., NCQ~\cite{Intel2003}) is also limited and a device firmware reorders I/O commands based on the internal geometry of storage media for improving device throughput~\cite{Yu2010,Filip2013}. Hence, a foreground I/O can be staged because the queue slots are busy handling background I/Os. Furthermore, even if a foreground I/O can be dispatched to the device, the device-internal scheduling can delay the processing of the I/O because of its internal scheduling policy. 

\vspace{-0.1in}
\subsection{I/O Priority Inversion}\label{sec:Challenge2}
\vspace{-0.05in}

% I/O priority inversion problem
The most straightforward way of improving application performance in the existence of background tasks would be to prioritize foreground I/Os over background ones and all I/O layers respect their priorities. However, this simple prioritization is insufficient since I/O priority inversions caused by runtime dependencies can delays the execution of a foreground task (Figure~\ref{fig:overview}). Similar to the priority inversion problem in CPU scheduling~\cite{Lampson1980}, I/O priority inversions are problematic because the processing of a background I/O on which a foreground task depend can be arbitrarily delayed by other background I/Os.

% Causes of I/O priority inversion: task dependency
Two types of dependencies cause I/O priority inversions: a \textit{task dependency} and an \textit{I/O dependency}. The task dependency occurs when two tasks interact with each other via synchronization primitives, such as a lock and a condition variable. The dependency caused by a lock complicates effective I/O prioritization because a background task can be blocked waiting for an I/O within a critical section that a foreground task needs to enter. For instance, a foreground task attempting to write a file can be blocked on an inode mutex if the mutex is already held by a background task concurrently writing to the different part of that file. Likewise, the dependency caused by a condition variable also cause a similar problem. A foreground task should indirectly wait for the I/Os awaited by a background task that is going to signal the foreground task. For example in Linux ext4, when a foreground task calls \texttt{fsync()}, it waits on a specific condition variable which is signaled by jbd2 kernel thread, which could be busy completing journal transactions for background tasks.

% Causes of I/O priority inversion: I/O dependency
Meanwhile, the I/O dependency occurs between a task and an outstanding I/O. Basically, the I/O dependency is generated when a task needs to directly wait for the completion of an ongoing I/O in order to ensure correctness and/or durability. For example, when a foreground task calls \texttt{fsync()}, it blocks on the completion of a write I/O that is asynchronously issued by a kernel thread (e.g., pdflush in Linux) for cleaning buffer cache. Once the task and the I/O dependency-induced priority inversions occur, the foreground task should wait for a long time because each layer in the I/O path can arbitrarily prolong the processing of low-priority background I/Os.
 
% Difficulties of dependency resolution
Unfortunately, resolving I/O priority inversions is challenging for the following reasons. Firstly, dependency relationships cannot be statically determined since they depend on various runtime conditions, such as execution timing, resource constraint, and client requirement. For example, a foreground task does not always depend on the progress of a kernel thread handling file system transaction since the kernel thread periodically writes out transactions in background~\cite{ext4}. Secondly, dependency occurs in a transitive manner involving multiple concurrent tasks blocked at either synchronization primitives or various I/O stages in multiple layers. We empirically found that a dependency sometimes cascaded in four steps due to the complex interaction between delayed allocation and crash-consistency mechanism in a file system (Section~\ref{sec:Design4}). Finally, a dependency relationship might not be visible at the kernel-level because of the extensive use of user-level synchronizations (e.g., shared memory mutex) based on kernel-level supports (e.g., Futex~\cite{futex}) in modern applications.

\vspace{-0.1in}
\subsection{Related Work}\label{sec:RelatedWork}
\vspace{-0.05in}

Table~\ref{tab:challenges} summarizes how the illustrated challenges are addressed (or not) by the existing prioritization schemes. CFQ~\cite{CFQ} is a block-level I/O scheduler that supports multiple priority classes (real-time, best-effort, and idle) and priority levels (0 to 7)~\cite{ionice}. However, CFQ prioritizes I/Os only at the block-level queue. It does not consider the I/O priority inversion problem as well as the prioritization at the block queue admission stage.

Redline~\cite{Yang2008} adapts all I/O layers to limit the interference from background tasks (e.g., virus scanner) for improving responsiveness of interactive applications (e.g., web browser). Redline, however, lacks resolving I/O priority inversions that occur between foreground and background tasks in typical data-intensive applications.

Recently, Split~\cite{Yang2015}, a cross-layer I/O scheduling framework, is introduced to address the limitation of a single-level I/O schedulers. Basically, Split provides additional hooks to several layers for supporting correct cause mapping, cost estimation, and reordering, in the existence of the file system challenges like delayed allocation and journaling~\cite{Yang2015}. Based on the proposed framework, Split-AFQ and Split-Deadline have been implemented to prove its effectiveness. Split-AFQ, a priority-based scheduler using the Split framework, schedules write I/Os including \texttt{write()} and \texttt{fsync()} at the system-call layer to avoid the runtime dependencies caused by file system journaling. Different from conventional deadline schedulers, Split-Deadline provide deadline scheduling of \texttt{fsync()} calls. In addition, it aggressively writes-back dirty data in background to make the latency of \texttt{fsync()} more deterministic by minimizing the file system transaction entanglement. Though Split itself is a generic I/O scheduling framework, its representative schedulers do not specifically consider the I/O priority inversion problem despite its significance.

On the other side, QASIO~\cite{Jeong2015} considers I/O priority inversions for improving system responsiveness. However, QASIO solely focuses on the kernel-level dependencies to asynchronous writes based on the analysis of the several mobile app scenarios. Furthermore, sCache~\cite{Kim2015} fully considers I/O priority inversions at the kernel-level in order to effectively utilize non-volatile write caches. Both QASIO and sCache, however, do not consider I/O priority inversions at the user-level. Moreover, they do not address the challenges in the I/O path for effective I/O prioritization.

Though several challenges have been separately addressed in the previous work, we argue that only a holistic approach can deliver consistently high application performance as in Figure~\ref{fig:motiv}. This is because the I/O priority inversion problem can be worsened when combined with multiple layers as a dependency transitively occurs across layers. Our scheme (RCP) addresses all the challenges in Table~\ref{tab:challenges} by enlightening the I/O path and resolving the kernel- and user-level I/O priority inversions.

\begin{table}[t]
\begin{center}
\vspace{-0.1in}
\begin{footnotesize}
\begin{tabular}{|c||c|c|c|c|c|}
\hline
\multirow{3}{*}{\textbf{Scheme}} & \multicolumn{3}{|c|}{\textbf{Multiple}} & \multicolumn{2}{|c|}{\textbf{I/O Priority}} \\
& \multicolumn{3}{|c|}{\textbf{Independent Layers}} & \multicolumn{2}{|c|}{\textbf{Inversion}} \\
\cline{2-6}	
& Cache & Filesystem & Block & Kernel & User \\
\hline
\textbf{CFQ~\cite{CFQ}}& No & No & Yes & No & No \\
%\hline
%\hline
\textbf{Redline~\cite{Yang2008}} & Yes & Yes & Yes & No & No \\
\textbf{Split~\cite{Yang2015}} & Yes & Yes & Yes & No & No \\
\textbf{QASIO~\cite{Jeong2015}} & No & No & Yes & Yes & No \\
%\hline
\textbf{sCache~\cite{Kim2015}} & No & No & No & Yes & No \\
%\hline
\textbf{RCP} & Yes & Yes & Yes & Yes & Yes \\
\hline
\end{tabular}
\end{footnotesize}
\end{center}
\vspace{-0.2in}
\caption{\textbf{I/O prioritization challenges.} \textit{This table shows whether a specific challenge for effective I/O prioritization is addressed or not in each previous work.}}
\vspace{-0.2in}
\label{tab:challenges}
\end{table}

