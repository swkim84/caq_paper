%\blockquote{\textit{"... There are always cleaners and compaction threads that need to do I/O, but shouldn't hold off the higher-priority "foreground" I/O. ... Facebook really only needs two (or few) priority levels: low and high."}}
%In this paper, without loss of generality, we use task to refer to a single thread of execution in a computing system, being it either a thread in a multi-threaded application, or a process.

\vspace{-0.1in}
\section{Our Approach}\label{sec:Approach}
\vspace{-0.1in}

In this work, we classify I/Os into two priority levels: (performance) \textit{critical} and \textit{non-critical} I/Os. In particular, we define a critical I/O as an I/O in the critical path of request handling since the response time of a request determines the level of application performance.

The proposed classification is distinguished from conventional I/O classification schemes: \textit{I/O-centric} and \textit{task-centric} classifications (Figure~\ref{fig:io-classification}). The I/O-centric classification differentiates the priority of each I/O based on its operation type (e.g., synchronous I/Os over asynchronous ones~\cite{Ganger1993,CFQ,Jeong2015}). On the other side, the task-centric classification distinguishes the I/Os based on issuing tasks (e.g., foreground I/Os over background ones~\cite{Yang2008}). These static classification schemes, however, are inadequate for identifying the (performance) criticality of I/Os. In our request-centric viewpoint, synchronous I/Os (e.g., checkpoint writes) and foreground I/Os (e.g., buffered writes) can be non-critical whereas asynchronous I/Os and background I/Os can sometimes be critical due to the runtime dependencies.
 
Based on the I/O criticality classification, we introduce a \textit{request-centric I/O prioritization} (or RCP) that identifies critical I/Os and prioritizes them over non-critical ones along the I/O path. This form of two-level I/O prioritization is effective for many cases since background tasks are ubiquitous in practice. For example, according to a Facebook developer: \enquote{\textit{... There are always cleaners and compaction threads that need to do I/O, but shouldn't hold off the higher-priority "foreground" I/O. ... Facebook really only needs two (or few) priority levels: low and high.}}~\cite{LSFMM2014a}.

Our goals for realizing RCP are twofold: 1) minimizing application modification for detecting critical I/Os, and 2) processing background tasks in a best-effort manner while minimizing the interference to foreground tasks. The following section describes our design for effectively identifying and enforcing I/O criticality throughout the I/O path.

\vspace{-0.1in}
\section{I/O Path Enlightenment}\label{sec:Design}

\vspace{-0.1in}
\subsection{Enlightenment API}\label{sec:Design1}
\vspace{-0.05in}
 
The first step to identifying critical I/Os is to track a set of tasks (i.e., foreground tasks) involved in request handling and this can be done in two ways: system-level and application-guided approaches. A system-level approach infers foreground tasks by using information available in the kernel. Though this approach has the benefit of avoiding application modification, it may induce runtime overhead for the inference and the possibility of misidentification. In contrast, an application-guided approach can accurately identify foreground tasks without runtime overheads at the expense of application modification. 
 
We chose the application-guided approach for accurate detection of foreground tasks without burdening the OS kernel. In particular, we provide an enlightenment interface to user-level so that an application developer (or an administrator if possible) can dynamically set/clear a foreground task based on application-level semantics. The foreground task can be from a short code section to an entire life of a thread depending on where set/clear APIs are called. The simplicity of using APIs makes developers easily prioritize critical I/Os in their applications. We believe the modification cost is also low in practice because typical data-intensive applications already distinguish foreground tasks from background tasks; see Section~\ref{sec:Applications} for the details. 

Since the API is solely used for deciding I/O criticality in the OS kernel, a wrong API call does not affect the correct execution of an application. However, API abuse by a malicious or a thoughtless application/tenant may compromise performance isolation among multiple applications/tenants sharing a storage stack. This problem can be solved by integrating RCP to a group-based resource management (e.g., cgroup in Linux~\cite{Menage2007}). Addressing this issue is out of scope of this paper.

\begin{figure}
\centering
\vspace{-0.1in}
\epsfig{file=../figures/io-classification.eps, width=2.5in}
\vspace{-0.1in}
\caption{\textbf{Comparison with conventional approaches.} \textit{}}
\vspace{-0.15in}
\label{fig:io-classification}
\end{figure}

\vspace{-0.1in}
\subsection{I/O Priority Inheritance}\label{sec:Design2}
\vspace{-0.05in}

% Dependency-induced critical I/Os
Based on the enlightenment API, we basically regard a synchronous I/O issued by a foreground task as a critical I/O. This obvious identification, however, is insufficient for detecting all critical I/Os because runtime dependencies cause background I/Os to be awaited by foreground tasks indirectly (Section~\ref{sec:Challenge2}). Hence, the next step for critical I/O detection is to handle I/O priority inversions caused by runtime dependencies. To this end, we introduce \textit{I/O priority inheritance} that temporarily gives critical I/O priority to a background task (Section~\ref{sec:Design2-1}) or a background I/O (Section~\ref{sec:Design2-2}) on which a foreground task depends to make progress.

\vspace{-0.1in}
\subsubsection{Handling Task Dependency}\label{sec:Design2-1}
\vspace{-0.05in}

% I/O priority inheritance on kernel-level synchronizations
\textbf{Kernel-level dependency.} Resolving the lock-induced dependency has been well-studied in the context of CPU scheduling~\cite{Lampson1980}. Inspired by the previous work, we resolve the lock-induced dependency by inheriting critical I/O priority to a background task when it blocks a foreground task until it leaves a critical section. Specifically, we record an owner task into each lock object (e.g., mutex). When a task is blocked to acquire a lock, the lock owner inherits I/O priority of the waiting task. The inherited priority is revoked when the owner task exits the critical section. This inheritance procedure is repeated until the foreground task acquires the lock. Note that we consider only blocking-based locks since spinning-based locks  are not involved with I/O waiting.

Unlike the case of locks, there is no distinct owner in a condition variable at the time of dependency occurs. To deal with the condition varible-induced dependency, we borrow a solution in previous work~\cite{Cucinotta2009}. In particular, a task is registered as a \textit{helper task}~\cite{Cucinotta2009} into the corresponding object of a condition variable when the task is going to signal the condition variable. Later, the helper task inherits I/O priority of a task blocked to wait for the condition become true. The inherited I/O priority is revoked when the helper task finally signals the waiting task. In the kernel-level, this approach is viable because only a few condition variables and threads cause the runtime dependencies.

% I/O priority inheritance on user-level synchronizations
\textbf{User-level dependency.} The key challenge in handling the user-level dependency is that the OS kernel cannot clearly identify a dependency relationship resulted from user-level synchronizations. For user locks, the kernel cannot determine the owner because a user lock variable is located in a shared memory region and modified through atomic operations (e.g., \texttt{cmpxchg} in x86) to indicate the lock status. This is an intrinsic optimization to eliminate unnecessary kernel interventions in the uncontended cases. The kernel is involved only when to block or to wake up lock waiters via a system call (e.g., \texttt{sys\_futex()}). As a consequence, the OS kernel can see only the waiters failed to acquire a user-level lock.

To detect the owner of a user lock, we adjust a user lock primitive (e.g., \texttt{pthread\_mutex\_lock()}) to additionally pass down the information of the owner when a task should block in the kernel due to lock contention. In practice, this can be readily done without modifying existing interfaces; see Section~\ref{sec:Implementation} for our implementation. Based on the delivered information, the kernel can properly inherit the I/O priority of a waiting task to a lock-holder task. Note that this modification does not entail the kernel intervention in uncontended cases.

Unlike the condition variable-induced dependency in kernel-level, handling such dependency in user-level is difficult because it is hard to pinpoint helper tasks for condition variables. Modern applications extensively use user-level condition variables for various purposes. For instance, we found over a hundred of user-level condition variables in the source code of MongoDB. Therefore, properly identifying all helper tasks is not trivial even for an application developer.

We adopt an inference technique that identifies a helper task based on usage history of each user-level condition variable. Typically, a background task is dedicated to a specific activity like logging, checkpointing, compaction, and buffer cleaning. Hence, a task signaling a condition is highly likely signal the condition again. Based on this observation, a background task is registered as a helper task when it signals a user-level condition variable. Then, the helper task inherits critical I/O priority of a foreground task when the foreground task needs to block on the user-level condition variable. The helper task is unregistered when it does not signal again for a specific time window.

\vspace{-0.1in}
\subsubsection{Handling I/O Dependency}\label{sec:Design2-2}
\vspace{-0.05in}

% Difficulty
Properly resolving a dependency to an outstanding I/O is complicated because the dependent I/O can be in any stage in the block layer at the time of the dependency occurs. For example, an outstanding I/O can be in admission control stage waiting for the available slots of a block-level queue. Hence, we need to track the status of an ongoing non-critical I/O and appropriately reprioritize it according to the current location when required.

% I/O Tracking
For tracking outstanding non-critical I/Os, we add an \texttt{ncio} data structure that stores an I/O descriptor, current location, and the descriptor of a requesting task. An \texttt{ncio} object is allocated when an incoming I/O is classified as non-critical at the entrance of the block layer, and inserted to a per-device list indexed by starting sector number. The fields including I/O descriptor and current location in the \texttt{ncio} object are properly updated as the corresponding I/O flows along the I/O path. The allocated \texttt{ncio} object is freed when the corresponding I/O is reclassified or dispatched to a device. 

% Resolution
When a dependency to an ongoing non-critical I/O occurs, the per-device \texttt{ncio} list is searched to find the corresponding \texttt{ncio} object. Then, the non-critical I/O is reclassified as critical I/O based on the information in the I/O descriptor stored in an \texttt{ncio} object if the corresponding \texttt{ncio} object is found. In this case, we may need to conduct additional chores according to the current location of the dependent I/O; we present the details in the following subsection.

% Overhead
The runtime overhead for maintaining the \texttt{ncio} structure is fairly small. In our implementation, we used a red-black tree for fast lookup. The memory cost is also limited because the number of outstanding non-critical I/Os is limited (128 by default) by the admission control mechanism at the block layer.

\vspace{-0.1in}
\subsection{Criticality-Aware I/O Prioritization}\label{sec:Design3}
\vspace{-0.05in}

As we discussed in Section~\ref{sec:Challenge1}, prioritizing critical I/Os only within a single layer (e.g., scheduling in a block-level queue) is ineffective for improving application performance. Hence, we adapt each layer in the I/O path to make it understand and enforce the I/O criticality.

% Caching layer
In the caching layer, similar to the approach in \cite{Yang2008}, we apply separate dirty ratios to tasks issuing critical and non-critical writes, respectively. For tasks issuing non-critical writes, the applied dirty ratio is low (1\% by default) to mitigate the interference to foreground tasks. With the lowered limit, a background task writing a large amount of data to buffer cache cannot fill all the available space since it should block until the current dirty ratio drops below the configured ratio. As a consequence, a foreground task is not blocked by a burst of background writes. Moreover, a foreground task calling \texttt{fsync()} does not need to depend on a large amount of dirty data generated by background tasks resulting from the file system ordering requirement~\cite{Yang2008,Yang2015}.

% Block layer: admission
In the admission control stage at the block layer, we separately allocate the block queue slots for critical and non-critical I/Os, respectively, so that the admission control is isolated between critical and non-critical I/Os. To resolve the dependency to the I/O blocked at this stage, we transiently give critical I/O priority to the requesting task recorded in the corresponding \texttt{ncio} object and wake up the task to make it retry allocation of available slot with critical I/O priority. By doing so, the criticality-inherited I/O can avoid the unnecessary congestion with other non-critical I/Os during the admission. 

% Block layer: scheduling
We also designed a simple priority-based I/O scheduler at the block layer. In particular, our scheduler maintains two FIFO queues that are dedicated for critical and non-critical I/Os each. Then, all I/Os in the critical queue is dispatched first before any I/O in the non-critical queue. To prevent starvation, we use a timer to monitor the non-critical queue and guarantee that at least one non-critical I/O is processed per unit of time (10 ms by default). Furthermore, we added queue promotion support into our scheduler for properly handling a dependency to a non-critical I/O staged at the block-level queue. In order to minimize the interference at the device-level, we conservatively limit the number of non-critical I/Os dispatched to a storage device; this number is configurable and we use one by default. This form of limiting is common in practice for improving responsiveness~\cite{CFQ,Losh2015}. Our scheme can be integrated with an existing feature-rich scheduler like Linux CFQ at the expense of additional work to support the I/O priority inheritance.

%Note that a foreground task trying to write can block at the admission control stage in the caching layer when the dirty ratio has been eventually reached the global threshold (e.g., 20\% by default in Linux). In this case, asynchronous writeback I/Os for cleaning buffer cache should be processed quickly. Therefore, our scheduler proactively moves a configurable number of non-critical writes to the critical queue before reaching the high dirty threshold for maintaining the number of dirty pages in buffer cache below the high threshold. 

%\begin{figure}
%\centering
%\epsfig{file=../figures/transitive-dependency.eps, width=2.7in}
%\vspace{-0.1in}
%\caption{\textbf{Handling transitive dependency.} \textit{A thick line represents a critical path of request execution while dotted lines indicate blocked execution. A box represents the blocking status of the corresponding task. A thick arrow indicates specific actions described in the corresponding text. FG means a foreground task handling a client's requests.}}
%\vspace{-0.2in}
%\label{fig:transitive-dependency}
%\end{figure}

% Dependency tracking
\vspace{-0.1in}
\subsection{Handling Transitive Dependency}\label{sec:Design4}
\vspace{-0.05in}

Transitive dependencies make effective I/O prioritization more challenging. Consider a dependency chain of tasks ($\tau_{1}, \tau_{2}, ..., \tau_{n}$) where each task $\tau_{i} (1 \leq i \leq n-1)$ is blocked due to a task dependency to $\tau_{i+1}$. The last task $\tau_{n}$ can be in one of the three states: runnable, blocked due to I/O dependency, and blocked at the admission control stages. If $\tau_{n}$ is runnable or blocked due to the I/O dependency, the transitive dependency can be resolved by inheriting the critical I/O priority through the dependency chain. 

However, if $\tau_{n}$ is blocked at one of the admission stages, inheriting the critical I/O priority is insufficient because the cause of the blocking should also be resolved. To handle this case, the applied dirty ratio at the caching layer is transiently changed to that of critical I/Os and the blocked task is woken up. At the block layer, similar to the I/O dependency resolution, the critical I/O priority is transiently inherited by the blocked task and the task is woken up. Then, the awakened task retries the admission with the changed I/O priority. 

In order to resolve the transitive dependencies, we record a blocking status into the descriptor of a task when the task is about to be blocked. A blocking status consists of blocking cause and an object to resolve the cause. Blocking cause can be one of task dependency, I/O dependency, and blocking at admission control stage. For the task dependency cause, a corresponding object of lock or condition vatiable is recorded. For the I/O dependency cause, an I/O descriptor (i.e., block device identifier and sector number) is recorded. No additional object is recorded for the blocking at admission control stage. Based on the recorded blocking status, a foreground task can effectively track and resolve the blocking causes in the transitive dependencies. 

In our experiments, at most four steps of transitive dependency has occurred. In particular, a foreground task is blocked on an inode mutex for file writes. The mutex is held by a background task and the task is also blocked waiting for the signal by a journaling daemon since the task tries to open a new file system transaction. The journaling daemon is also waiting for the completion of updating journal handle by another background task. The last background task is blocked on the admission control stage of the block layer because the task is issuing a burst of writeback for carrying out delayed allocation.

%Figure~\ref{fig:transitive-dependency} illustrates an example to describe how the transitive dependencies are handled: (1) foreground task T1 attempts to wait on the cond where background task T2 is registered as the helper task. (2) Then, T2 inherits critical I/O priority from T1, and (3) T1 checks T2's blocking status. Since T2 is currently blocked for the lock held by background task T3, (4) T3 also inherits criticality and (5) T1 again inspects T3's blocking status. (6) Finally, T1 wakes up T3 in order to make T3 retry admission with boosted criticality because T3 is blocked waiting for being admitted to either write to buffer cache or enter to block-level queue. As a result, unnecessary delay in request execution can be avoided by properly resolving the transitive dependencies.

%Futex.   The futex() system call provides a method for waiting until a certain condition becomes true.  It is typically used as a blocking construct in the context of shared-memory synchronization.  When using futexes, the majority of the synchronization operations are performed in user space.  A user-space program employs the futex() system call only when it is likely that the program has to block for a longer time until the condition becomes true.  Other futex() operations can be used to wake any processes or threads waiting for a particular condition. Note that no explicit initialization or destruction is necessary to use futexes; the kernel maintains a futex (i.e., the kernel-internal implementation artifact) only while operations such as FUTEX\_WAIT, described below, are being performed on a particular futex word.
%In this paper, we consider the mutex implementation in pthread library since it is most widely used for multi-threaded applications to implement user locks. Since pthread library already provides a way of passing the owner information to the kernel (especially for implementing priority inheritance)~\cite{}, we exploit this support to resolve the user lock-induced dependency. Specifically, pthread library maintains the owner for each mutex and delivers the owner as a parameter to \texttt{sys\_futex()} when the lock is already acquired. 
% For the pthreads library, we implemented this strategy by overriding routines that could potentially cause a thread to block: pthread_mutex and pthread_cond_wait. To override a routine in a dynamically linked application, we use library preloading. That is, at program launch time, HPCToolKit injects a dynamically linked hooking library into an unmodified program's address space. For statically linked programs, compilation remains unchanged, but we require users to adjust thier likn step to invoke a script that adds HPCToolKit's profiling library to a statically linked executable via linkers's special --wrap option. When a monitored application calls one of the overriden routines, control is transferred to the monitored version of the routine. The override then sets a thread-local idleness flag and immediately calss the actual pthreads routine. When the thread enters the lock or condition variable critical section,  the pthreads routine returns to the override, which immediately clears the idleness flag and returns to the monitored application.
% __wait_on_bit: PG_locked(wait_on_page_locked, wait_on_page_locked_killable), PG_writeback(wait_on_page_writeback <= set_page_writeback()), BH_Lock(wait_on_buffer), BH_Shadow (do_get_write_access (=sleep_on_shadow_bh)).
% __wait_on_bit_lock: PG_locked(lock_page, lock_page_killable), BH_Lock(lock_buffer) 
% wait_queue_head_t       j_wait_transaction_locked: Wait queue for waiting for a locked transaction to start committing, or for a barrier lock to be released. helper task: jbd2
% wait_queue_head_t       j_wait_done_commit: Wait queue for waiting for commit to complete. helper task: jbd2
% wait_queue_head_t       j_wait_commit: Wait queue to trigger commit.
% wait_queue_head_t       j_wait_updates: Wait queue to wait for updates to complete. helper tasks: dynamic.
% wait_queue_head_t       j_wait_reserved: Wait queue to wait for reserved buffer credits to drop.
% Task dependency: lock - mutex, rw_semaphore, user lock(futex, sys v sem), cond - j_wait_transaction_locked (helper=jbd2), j_wait_done_commit(helper=jbd2), j_wait_updates(helper=dynamic).
%A critical process can attempt modify metadata buffer, which was marked shadow buffer by jbd2, corresponding to sector 100 for example. In this case, the critical process should wait for the completion of journal I/O. Moreover, jbd2 also tries to write out other journal buffers (e.g., sector 92), it can be blocked at block layer for getting a free request descriptor, thereby making the critical process blocked more.
