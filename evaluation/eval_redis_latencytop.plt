#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 26
#set size 1,1
#set key invert reverse left Left width -1
set key invert top vertical width -1 height 0 samplen 2 font "Times-Roman, 24"
set ylabel 'Total wait time (min)' offset 2,0 
#set xrange [:9]
set yrange [0:500]
#set xtics 0,200000
set xtics nomirror
set xtic rotate by -30
set xtics ("CFQ" 0, "CFQ-IDLE" 1, "SPLIT-A" 2, "SPLIT-D" 3, "RCP" 4) font "Times-Roman, 24"
#set label "38% reduction\nin block-wait" at 0.7, 873485.4
#set ytics 0,50
set ytics nomirror
#set ytics ('200' 200000, '400' 400000, '600' 600000, '800' 800000)
set style data histograms
set style histogram rowstacked
set grid y
set boxwidth 0.7
#set logscale y
#set format y '10^%T'

set size 1,1
set output '../figures/eval-redis-latencytop.eps'
plot 'eval_redis_latencytop.dat' using ($2/1000000/60) t 'jbd2\_wait\_commit' lt 1 fs solid 0.10,\
	 '' using ($3/1000000/60) t 'pg\_writeback' lt 1 fs pattern 1, \
	 '' using ($4/1000000/60) t 'write\_entry'  lt 1 fs solid 0.40, \
	 '' using ($5/1000000/60) t 'fsync\_entry'  lt 1 fs pattern 4, \
	 '' using ($6/1000000/60) t 'etc' lt 1 fs solid 0.70
