#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 26
#set key invert top vertical width -1 height 0 samplen 2 font "Times-Roman, 22"
set key invert top horizontal outside width -3 height 0 samplen 2 font "Times-Roman, 22"
set ylabel 'Throughput (MB/s)' offset 1,0 
set y2label 'Total wait time (sec)' offset -1,0 
set yrange [0:2100]
set y2range [0:210]
set xtics nomirror
set xtic rotate by -30
set xtics ("SOLO" 0, "MIXED" 1, "CFQ-PRIO" 2, "SPLIT-A" 3) font "Times-Roman, 22"
set ytics 0,300
set y2tics 0,30
set ytics nomirror
set style data histograms
set style histogram rowstacked
set grid y
set boxwidth 0.7

set size 1,0.8
set output '../figures/caching-layer.eps'
plot 'caching_layer.dat' using ($2/1024) t 'CP' lt 1 fs solid 0.10, '' using ($3/1024) t 'NCP'  lt 1 fs solid 0.60, '' using ($4/1000000) t 'balance\_dirty\_pages wait' with linespoints lt 1 pt 3 lw 2 ps 2 axis x1y2
