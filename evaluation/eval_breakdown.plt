#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 30 
set ylabel "Normalized throughput" offset 1,0
#set xlabel "Application" #offset 1.5,0

set style data histograms
set style histogram
set style histogram errorbars lw 2 gap 1
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key top horizontal outside width -2 height 1 samplen 2 font "Times-Roman, 26"
#set key top horizontal samplen 2 font "Times-Roman, 26"

set yrange [0:1.1]
set xrange [-0.5:1.5]
set xtics nomirror
set ytics nomirror 0,0.2
set format y "%.1f"
set grid y

set rmargin 1.2
set lmargin 6
set tmargin 2.5
set bmargin 1.5
#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

set size 1.1,0.8
set output "../figures/eval-breakdown.eps"
plot "eval_breakdown.dat" using ($2/$7):($8/$7):xtic(1) fs solid 0.1 title columnheader(2) ls 1, \
	"" using ($3/$7):($9/$7):xtic(1) fs pattern 1 title columnheader(3) ls 1,\
	"" using ($4/$7):($10/$7):xtic(1) fs solid 0.4 title columnheader(4) ls 1, \
	"" using ($5/$7):($11/$7):xtic(1) fs pattern 2 title columnheader(5) ls 1,\
	"" using ($6/$7):($12/$7):xtic(1) fs solid 0.7 title columnheader(6) ls 1, \
	"" using ($7/$7):($13/$7):xtic(1) fs pattern 4 title columnheader(7) ls 1, \
#plot "performance_small.dat" using ($2/1000):($8/1000):xtic(1) fs pattern 1 title columnheader(2) ls 1, \
#	"" using ($3/1000):($9/1000):xtic(1) fs solid 0.1 title columnheader(3) ls 1,\
#	"" using ($4/1000):($10/1000):xtic(1) fs pattern 2 title columnheader(4) ls 1, \
#	"" using ($5/1000):($11/1000):xtic(1) fs solid 0.4 title columnheader(5) ls 1,\
#	"" using ($6/1000):($12/1000):xtic(1) fs pattern 4 title columnheader(6) ls 1, \
#	"" using ($7/1000):($13/1000):xtic(1) fs solid 0.8 title columnheader(7) ls 1
