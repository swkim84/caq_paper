#!/opt/local/bin/gnuplot

#set terminal png enhanced font 'Times-Roman' 22
set terminal postscript eps dashed color 'Times-Roman' 30
#set terminal postscript eps enhanced monochrome
#set terminal post 'Times-Roman' 24
set ylabel "CCDF P[X >= x]" offset 1,0
set xlabel "Transaction latency (ms)" offset 0,0.6

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key outside top horizontal width 1 height 0 samplen 2 font "Times-Roman, 26"

set rmargin 1.2
set lmargin 6
set tmargin 2.5
set bmargin 2.5

set yrange [:1]
#set xrange [:8]
set xtics nomirror offset 0,0.2
set ytics nomirror
set logscale y
set logscale x
set format y '10^{%T}'
set format x '10^{%T}'
#set format x '%.1f'
#set grid y
#set grid x

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

set size 1.1,0.8
set output "../figures/eval-postgres-latency-2hdds.eps"
plot "eval_postgres_latency_2hdds.dat" using ($2/1000):1 with linespoints lc rgb 'gray' lw 5 title 'CFQ',\
	"" using ($3/1000):1 with linespoints lc rgb 'green' lw 5 title 'CFQ-IDLE',\
	"" using ($4/1000):1 with linespoints dt 3 lc rgb 'blue' lw 5 title 'SPLIT-A',\
	"" using ($5/1000):1 with linespoints dt 4 lc rgb 'red' lw 5 title 'SPLIT-D',\
	"" using ($6/1000):1 with linespoints lc rgb 'black' lw 5 title 'RCP'
