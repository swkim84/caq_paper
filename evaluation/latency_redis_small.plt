#!/opt/local/bin/gnuplot

#set terminal png enhanced font 'Times-Roman' 22
set terminal postscript eps dashed color 'Times-Roman' 24
#set terminal postscript eps enhanced monochrome
#set terminal post 'Times-Roman' 24
set ylabel "Operation latency (sec)" #offset 1,0
set xlabel "%-ile" offset 0,0

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key outside top horizontal width 1 height 0 samplen 3 font "Times-Roman, 24"

set yrange [-1:]
#set xrange [0:120]
set xtics nomirror ('90th' 0, '99th' 1, '99.9th' 2, '99.99th' 3, '99.999th' 4)
set ytics nomirror
#set logscale y
#set format y '10^%T'
#set grid y
#set grid x

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

set size 1,0.8
set output "../figures/latency-redis-small.eps"
plot "latency_redis_small.dat" using 1:($2/1000000) with lines lc rgb 'gray' lw 5 title 'CFQ',\
	"" using 1:($3/1000000) with lines lc rgb 'green' lw 5 title 'CFQ-IDLE',\
	"" using 1:($4/1000000) with lines dt 3 lc rgb 'blue' lw 3 title 'SPLIT-D',\
	"" using 1:($5/1000000) with lines dt 4 lc rgb 'red' lw 3 title 'SPLIT-A',\
	"" using 1:($6/1000000) with lines lc rgb 'black' lw 5 title 'RCP'

