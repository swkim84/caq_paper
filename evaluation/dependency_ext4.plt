#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 24
set ylabel "Wait time (sec)" offset 1.5,0

set style data histograms
set style histogram
set style fill solid border -1
set boxwidth 0.75

set key top horizontal outside width -4 height 0 samplen 2 font "Times-Roman, 24"

set yrange [1:]
set xtics nomirror
set xtics ("" 0) font "Times-Roman, 22"
set ytics nomirror
set logscale y 10
set grid y
set size 1,0.8

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5


set xrange [-0.4:0.5]
set xlabel "41.28\% slowdown" offset 0,1
set output "../figures/dependency-ext4-postgres.eps"
plot "dependency_ext4_postgres.dat" using ($2/1000000) fs pattern 1 title 'mutex\_lock' ls 1, \
	"" using ($3/1000000) fs solid 0.1 title 'wait\_transaction\_locked' ls 1,\
	"" using ($4/1000000) fs pattern 2 title 'semtimedop' ls 1, \
	"" using ($5/1000000) fs solid 0.4 title 'jbd2\_log\_wait\_commit' ls 1,\
	"" using ($6/1000000) fs pattern 4 title 'sleep\_on\_shadow\_bh' ls 1, \
	"" using ($7/1000000) fs solid 0.8 title 'etc' ls 1

set xrange [-0.3:0.5]
set xlabel "65.08\% slowdown" offset 0,1
set key top horizontal outside width -2 height 0 samplen 2 font "Times-Roman, 24"
set output "../figures/dependency-ext4-mongodb.eps"
plot "dependency_ext4_mongodb.dat" using ($2/1000000) fs pattern 1 title 'mutex\_lock' ls 1, \
	"" using ($3/1000000) fs solid 0.1 title 'futex\_wait' ls 1,\
	"" using ($4/100000) fs pattern 2 title 'etc' ls 1, \
