#!/opt/local/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 24
set ylabel "Wait time (sec)" offset 1.5,0
set xlabel "88.10\% slowdown" offset 0,1

set style data histograms
set style histogram
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key top horizontal outside width -7 height 0 samplen 2 font "Times-Roman, 24"

set yrange [1:]
set xtics nomirror
set xtics ("" 0) font "Times-Roman, 22"
set ytics nomirror
set logscale y 10
set grid y
set size 1,0.8

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5


set xrange [-0.4:0.5]
set output "../figures/dependency-btrfs-postgres.eps"
plot "dependency_btrfs_postgres.dat" using ($2/1000000) fs pattern 1 title 'mutex\_lock' ls 1, \
	"" using ($3/1000000) fs solid 0.1 title 'semtimedop' ls 1,\
	"" using ($4/1000000) fs pattern 2 title 'wait\_on\_page\_writeback' ls 1, \
	"" using ($5/1000000) fs solid 0.4 title 'wait\_current\_trans' ls 1,\
	"" using ($6/1000000) fs pattern 4 title 'etc' ls 1
