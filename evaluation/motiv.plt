#!/opt/local/bin/gnuplot

#set terminal png enhanced font 'Times-Roman' 22
set terminal postscript eps dashed color 'Times-Roman' 24
#set terminal postscript eps enhanced monochrome
#set terminal post 'Times-Roman' 24
set ylabel "Operation thput (Kops/sec)" offset 1,0
set xlabel "Elapsed time (min)" offset 0,0

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key outside top horizontal width 1 height 0 samplen 3 font "Times-Roman, 24"

set yrange [0:30]
#set xrange [0:120]
set xtics nomirror #0,5
set ytics nomirror

#set format y '%.1f'
#set grid y
#set grid x

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

set size 1,0.8
set output "../figures/motiv.eps"
plot "motiv.dat" using ($1/60):($2/1000) with lines lc rgb 'gray' lw 8 title columnheader(2),\
	"" using ($1/60):($3/1000) with lines lc rgb 'green' lw 6 title columnheader(3),\
	"" using ($1/60):($4/1000) with lines dt 3 lc rgb 'blue' lw 3 title columnheader(4),\
	"" using ($1/60):($5/1000) with lines dt 4 lc rgb 'red' lw 3 title columnheader(5),\
	"" using ($1/60):($6/1000) with lines dt 5 lc rgb 'orange' lw 3 title columnheader(6),\
	"" using ($1/60):($7/1000) with lines lc rgb 'black' lw 4 title columnheader(7)

set output "../figures/motiv1.eps"
plot "motiv.dat" using ($1/60):($2/1000) with lines lc rgb 'gray' lw 4 title columnheader(2),\
	"" using ($1/60):($7/1000) with lines lc rgb 'black' lw 3 title columnheader(6)
set output "../figures/motiv2.eps"
plot "motiv.dat" using ($1/60):($3/1000) with lines lc rgb 'gray' lw 4 title columnheader(3),\
	"" using ($1/60):($4/1000) with lines lc rgb 'black' lw 3 title columnheader(4),\
	"" using ($1/60):($5/1000) with lines dt 3 lc rgb 'red' lw 5 title columnheader(5)
