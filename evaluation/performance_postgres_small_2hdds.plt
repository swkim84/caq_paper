#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome 'Times-Roman' 30
set output '../figures/performance-postgres-small-2hdds.eps'
#set size 0.95,1
#set bmargin 4
#set auto x
#set key invert reverse right Right width -1
set key invert font 'Times-Roman, 25' samplen 2
set ylabel 'Normalized throughput' offset 1,0 
#set xlabel 'Applied scheme' offset 0,1
set y2label 'Normalized latency' offset -1.5,0
#set xrange [-0.5:6]
set yrange [0.0:2.5]
set y2range [0.0:2.5]
#set xtics 0,10
set xtics nomirror font 'Times-Roman, 26'
set xtic rotate by -30
#set xtics ("32" 0, "128" 1, "512" 2, "1024" 3, "2048" 4, "4096" 5)
#set ytics 0,20
set ytics 0,0.5
set ytics nomirror
set y2tics 0,0.5
set style data histograms
set style histogram rowstacked
set grid y
set boxwidth 0.5
set format y '%.1f'
set format y2 '%.1f'
set size 1,0.8
#plot 'motiv.dat' using 2:xtic(1) title columnhead fs solid 0.3 lt 1 axis x1y2, '' using ($4/$3):xtic(1) title columnhead with linespoints lt 1 pt 3 lw 2 ps 2
plot 'performance_postgres_small_2hdds.dat' using ($3/$5):xtic(1) title columnhead fs solid 0.3 lt 1 axis x1y1, '' using ($2/$4):xtic(1) title columnhead with linespoints lt 1 pt 3 lw 3 ps 2 axis x1y2
#plot 'motiv_stalled_writes.dat' using 2 title columnhead fs solid 0.3 lt 1 axis x1y2, '' using 3 title columnhead with linespoints lt 1 pt 3 lw 2
