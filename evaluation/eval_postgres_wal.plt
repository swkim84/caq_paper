#!/opt/local/bin/gnuplot

#set terminal png enhanced font 'Times-Roman' 22
set terminal postscript eps dashed color 'Times-Roman' 30
#set terminal postscript eps enhanced monochrome
#set terminal post 'Times-Roman' 24
set ylabel "Trx log size (GB)" offset 2,0
set xlabel "Elapsed time (min)" offset 0,0.6

set style data histograms
set style fill solid border -1
set boxwidth 0.75

#set key top right horizontal samplen 2 width -8 height -0.5 font "Times-Roman, 18"
set key outside top horizontal width 1 height 0 samplen 2 font "Times-Roman, 26"

set yrange [0:30]
#set xrange [0:120]
set xtics nomirror offset 0,0.2
set ytics nomirror

set rmargin 1.2
set lmargin 5
set tmargin 2.5
set bmargin 2.5
#set format y '%.1f'
#set grid y
#set grid x

#set bmargin 1.5
#set tmargin 1
#set rmargin 1
#set lmargin 5.5

set size 1.1,0.8
#set size 1.1,1
set output "../figures/eval-postgres-wal.eps"
plot "eval_postgres_wal.dat" using ($1/60):($2/1024/1024) with lines lc rgb 'gray' lw 5 title columnheader(2),\
	"" using ($1/60):($3/1024/1024) with lines lc rgb 'green' lw 5 title columnheader(3),\
	"" using ($1/60):($4/1024/1024) with lines dt 3 lc rgb 'blue' lw 5 title columnheader(4),\
	"" using ($1/60):($5/1024/1024) with lines dt 4 lc rgb 'red' lw 5 title columnheader(5),\
	"" using ($1/60):($6/1024/1024) with lines dt 5 lc rgb 'orange' lw 5 title columnheader(6),\
	"" using ($1/60):($7/1024/1024) with lines lc rgb 'black' lw 5 title columnheader(7)

unset key
set tmargin 0.5
set size 1.1,0.7
set output "../figures/eval-postgres-wal-60gb.eps"
plot "eval_postgres_wal_60gb.dat" using ($1/60):($2/1024/1024) with lines lc rgb 'gray' lw 5 title columnheader(2),\
	"" using ($1/60):($3/1024/1024) with lines lc rgb 'green' lw 5 title columnheader(3),\
	"" using ($1/60):($4/1024/1024) with lines dt 3 lc rgb 'blue' lw 5 title columnheader(4),\
	"" using ($1/60):($5/1024/1024) with lines dt 4 lc rgb 'red' lw 5 title columnheader(5),\
	"" using ($1/60):($6/1024/1024) with lines dt 5 lc rgb 'orange' lw 5 title columnheader(6),\
	"" using ($1/60):($7/1024/1024) with lines lc rgb 'black' lw 5 title columnheader(7)
