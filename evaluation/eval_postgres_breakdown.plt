#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome 'Times-Roman' 30
set output '../figures/eval-postgres-breakdown.eps'
#set size 0.95,1
#set bmargin 4
#set auto x
#set key invert reverse right Right width -1
set key invert font 'Times-Roman, 30' samplen 2
set ylabel 'Transaction throughput (Ktrx/sec)' offset 1,0 
#set xlabel 'Applied scheme' offset 0,1
#set y2label 'Operation latency (ms)' offset 0,0
#set xrange [-1.0:5]
set yrange [0.0:8]
#set xtics 0,10
set xtics nomirror font 'Times-Roman, 30' #offset 1,0
#set xtic rotate by -30
#set xtics ("32" 0, "128" 1, "512" 2, "1024" 3, "2048" 4, "4096" 5)
#set xtics ("CFQ" 0, "CFQ-IDLE" 1, "SPLIT-A" 2, "SPLIT-D" 3, "RCP" 4)
#set ytics 0,20
#set ytics 0,0.2
set ytics nomirror
set style data histograms
set style histogram errorbars lw 2 gap 1
#set style histogram
set grid y
#set boxwidth 1.5
#set format y '%.1f'
#set format y2 '%.1f'
set size 1.2,1
set bmargin 3

plot 'eval_postgres_breakdown.dat' using ($2/1000):($3/1000):xtic(1) title columnhead fs solid 0.3 lt 1
