#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome 'Times-Roman' 30
#set size 0.95,1
#set bmargin 4
#set auto x
#set key invert reverse right Right width -1
set key invert horizontal font 'Times-Roman, 26' samplen 2 height -0.2
set ylabel 'System thput (MB/sec)' offset 2.5,0 
set xlabel '# limit on non-critical I/O' offset 0,0.5
set y2label 'Critical I/O latency (us)' offset -1,0
set xrange [-0.5:6.5]
set yrange [0.0:400]
set y2range [10:]
#set xtics 0,10
set xtics nomirror font 'Times-Roman, 26' #rotate by -20 #offset 1,0
#set xtic rotate by -30
#set xtics ("32" 0, "128" 1, "512" 2, "1024" 3, "2048" 4, "4096" 5)
#set xtics ("CFQ" 0, "CFQ-IDLE" 1, "SPLIT-A" 2, "SPLIT-D" 3, "RCP" 4)
#set ytics 0,20
#set ytics 0,0.2
set ytics nomirror 0,100 offset 0.5,0
set y2tics nomirror offset -0.5,0
set logscale y2
#set y2tics 0,0.5
set rmargin 5
set lmargin 5
set tmargin 1.5
set bmargin 2.5

set format y2 '10^%T'
set style data histograms
set style histogram errorbars lw 2 gap 1
#set style histogram
set grid y
#set boxwidth 1.5
#set format y '%.1f'
#set format y2 '%.1f'
set size 1.1,0.8
set output '../figures/eval-fio-performance.eps'
plot 'eval_fio_performance.dat' using ($2/1024):($5):xtic(1) title columnhead fs solid 0.3 lt 1 axis x1y1, '' using ($3) title columnhead with linespoints lt 1 pt 3 lw 3 ps 2 axis x1y2#, '' using ($4) title columnhead with linespoints lt 1 pt 4 lw 3 ps 2 axis x1y2
