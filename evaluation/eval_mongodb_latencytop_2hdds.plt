#!/usr/bin/gnuplot

set terminal postscript eps enhanced monochrome
set terminal post 'Times-Roman' 30
#set size 1,1
#set key invert reverse left Left width -1
#set key invert top vertical width -4 height 0 samplen 2 font "Times-Roman, 24"
set key invert at 5.14,100 vertical width -2 height 0 samplen 1 font "Times-Roman, 27"
set ylabel 'Total wait time (min)' offset 3,0 
set y2label 'Operation latency (ms)'offset -2.5,0
set xrange [-0.5:5.1]
set yrange [0:100]
set y2range [0:50]
set rmargin 5
set lmargin 5
set tmargin 0.5

#set xtics 0,200000
set xtics nomirror 
#set xtic rotate by -30
set xtics ("CFQ" 0, "CFQ-IDLE" 1, "SPLIT-A" 2, "SPLIT-D" 3, "RCP" 4) font "Times-Roman, 26" 
#set label "38% reduction\nin block-wait" at 0.7, 873485.4
#set ytics 0,50
set ytics nomirror offset 0.4,0
set y2tics nomirror offset -0.8,0
#set ytics ('200' 200000, '400' 400000, '600' 600000, '800' 800000)
set style data histograms
set style histogram rowstacked
set grid y
set grid y2
set boxwidth 0.6
#set logscale y
#set logscale y2
#set format y2 '10^%T'

set size 1.2,0.85
set output '../figures/eval-mongodb-latencytop-2hdds.eps'
plot 'eval_mongodb_latencytop_2hdds.dat' using ($2/1000000/60) t 'futex\_lock\_pi' lt 1 fs solid 0.10,\
	 '' using ($3/1000000/60) t 'mutex\_lock' lt 1 fs pattern 1, \
	 '' using ($4/1000000/60) t 'jbd2\_log\_wait'  lt 1 fs solid 0.40, \
	 '' using ($5/1000000/60) t 'futex\_wait'  lt 1 fs pattern 4, \
	 '' using ($6/1000000/60) t 'nanosleep' lt 1 fs solid 0.70, \
	 '' using ($7/1000000/60) t 'etc'  lt 1 fs pattern 7, \
	 '' using ($8/1000) t 'Avg latency' with linespoints lt 1 pt 3 lw 3 ps 2 axis x1y2
#	 '' using ($9/1000) t '99.9th-%ile latency' with linespoints lt 1 pt 4 lw 2 ps 2 axis x1y2
